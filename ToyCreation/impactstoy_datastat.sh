#!/bin/bash
# 0=speed, 1=balance, 2=robustness
STRAT=0
# Default: 0.1
TOL=0.01
# Default batch of settings:
Settings1="-m 125 --cminPreScan --cminFallbackAlgo Minuit2,Combined,2:0.3 --cminDefaultMinimizerTolerance $TOL --cminDefaultMinimizerStrategy $STRAT --setRobustFitStrategy $STRAT --X-rtd MINIMIZER_MaxCalls=999999999 --cminDefaultMinimizerPrecision 1E-12"
#Settings2="--redefineSignalPOIs MTop --freezeParameters r --setParameters MTop=0,r=1 --setParameterRanges MTop=-1,1 --robustFit 1 --stepSize=0.005 -v 2"

# allConstrainednuisances added
Settings2="--redefineSignalPOIs MTop --freezeParameters allConstrainedNuisances --setParameters MTop=0,r=1 --setParameterRanges MTop=-1,1 --robustFit 1 --stepSize=0.005 -v 2"


#mode="exp"
mode="obs"

if [[ "$mode" == "exp" ]]; then
    Sets="$Settings2 -t -1 $Settings1"
else
    Sets="$Settings2 $Settings1"
fi
wait
# Initial step:
combineTool.py -M Impacts $Sets -d space_${1}.root --doInitialFit > impactInitial.log
wait
# Loop over nuisances:
combineTool.py -M Impacts $Sets -d space_${1}.root --doFits --parallel 20 --cminPreFit 1
wait
# Collect:
combineTool.py -M Impacts -d space_${1}.root -m 125 -o imps.json --redefineSignalPOIs MTop --freezeParameters r
wait
# Dropping the dummy 'r' variable:
jq 'del(.params[] | select(.name == "r"))' imps.json > impacts.json
wait
rm imps.json
# Plot
#plotImpacts.py -i ../../impacts_${1}.json --POI MTop --units [GeV] -t ../../../../scripts/plotting/rename.json -o ${1}
#wait