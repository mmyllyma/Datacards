#!/bin/bash
Offset=0
NToys=100

for IToy in $(seq 1 $NToys); do
    echo "Toy ${IToy}"
    bash singletoy.sh $(($IToy+$Offset))
    wait
done
