#!/usr/bin/env python
from ROOT import *
from array import array
import numpy as np
from scipy import stats
import argparse
import glob
import sys
import json

#ROOT.PyConfig.IgnoreCommandLineOptions = False
gROOT.gPrintViaErrorHandler = True
gROOT.SetBatch(True)

import array
import collections
import math
import os.path
import optparse
import re
import subprocess

# private libs
import uncertainties as unc

divideByBinWidth = False
saveStuff = []

def addYears(target, yrs, added):
  for add in added:
    for yr in yrs:
      target.append(add + "_" + yr)

years = ["2016APV", "2016", "2017", "2018"]
nuisances = []
singleside = []
# Muon-only
#  ["MuoIDSF", "MuoIsoSF", "MuoIDSFStat", "MuoIsoSFStat", "MuoTrgSF"]
#nuisances += ["MuoScaleStat", "MuoScaleEWK", "MuoScaleEWK2", "MuoScaleZPt", "MuoScaleDM"]
# Electron-only
#nuisances += ["EleIDSF", "EleRecoSF", "EleTrgSF", "EleScale", "EleReso"]
nuisances = []
# FSR
nuisances += ["FSRx2xg", "FSRq2qg", "FSRg2gg", "FSRg2qq", "FSRCNSx2xg", "FSRCNSq2qg", "FSRCNSg2gg", "FSRCNSg2qq"]
# ISR
nuisances += ["ISR", "ISRst"]
# B-fragmentation
nuisances += ["BFragPeterson", "BFragBL", "BFragSL"]
singleside += ["BFragPeterson"]
# B-tagging
nuisances += ["BCTagSF_corr", "BMisTagSF_corr"]
addYears(nuisances, years, ["BCTagSF", "BMisTagSF"])
# Misc
nuisances += ["L1Prefire", "FactScale", "RenScale", "Pileup", "TopPt"]
# PDF
nuisances += ["AlphaS", "PDF", "CT14", "MMHT2014"]
singleside += ["CT14", "MMHT2014"]
# Flavor JEC
nuisances += ["FlavorPureBottom", "FlavorPureCharm", "FlavorPureQuark", "FlavorPureGluon"]
# 100% correlated JEC
nuisances += ["JECAbsoluteMPFBias", "JECAbsoluteScale", "JECFragmentation", "JECSinglePionECAL", "JECSinglePionHCAL"]
# 50% correlated JEC
nuisances += ["JECPileUpDataMC", "JECPileUpPtBB", "JECPileUpPtEC1", "JECPileUpPtEC2", "JECPileUpPtRef", "JECPileUptPtHF", "JECRelativeFSR", "JECRelativeJERHF", "JECRelativePtBB", "JECRelativePtHF", "JECRelativeBal"]
# 0% correlated JEC
addYears(nuisances, years, ["JECAbsoluteStat", "JECRelativeJEREC1", "JECRelativeJEREC2", "JECRelativePtEC1", "JECRelativePtEC2", "JECRelativeSample", "JECRelativeStatEC", "JECRelativeStatFSR", "JECRelativeStatHF", "JECTimePtEta"])
# JER/MET
addYears(nuisances, years, ["JERInner", "JERMid", "JEROuter", "MET"])
# Two-sided troublemakers
nuisances += ["HDAMP", "UETune"]
# One-sided troublemakers
nuisances += ["CRQCDBased", "CRGluonMove", "ERD"]
singleside += ["CRQCDBased", "CRGluonMove", "ERD"]

def dataLikeName(name):
  return "Data" in name or "Pseudodata" in name or "Direct" in name

def modifySaveName(name):
  replacements = {"(": "", ")": "", "&": "AND", ".": "p",
                  "/": "DIV", "<": "", ">": "", "*": "TIMES", "$": "DOLLAR"}
  for a, b in replacements.iteritems():
    name = name.replace(a, b)
  return name

def integerContent(h, scaledByWidth=False):
  for bin in loopH(h):
    c = h.GetBinContent(bin)
    if scaledByWidth:
      c *= h.GetBinWidth(bin)
    if abs(c - int(round(c))) > 1e-5:
      return False
  return True

def getPoissonUnc(n):
  # http://prd.aps.org/abstract/PRD/v86/i1/e010001 (p.399)

  # calculate x = 1-alpha ( approx 68% )
  x = TMath.Erf(1. / sqrt(2))
  alpha = 1 - x

  # for central confidence intervals, alpha_lo =alpha_up = alpha/2
  alpha_lo = alpha / 2
  alpha_up = alpha / 2

  # confidence interval is [ xlo, xup ]
  xlo = 0.5 * TMath.ChisquareQuantile(alpha_lo, 2 * n)
  xup = 0.5 * TMath.ChisquareQuantile(1 - alpha_up, 2 * (n + 1))
  return n - xlo, xup - n

def baseStyle():
  st = TStyle("defaultStyle", "Gangnam style")
  st.SetCanvasColor(kWhite)
  st.SetCanvasBorderMode(0)
  st.SetFrameBorderMode(0)
  st.SetCanvasDefH(800)
  st.SetCanvasDefW(800)

  st.SetPadTickX(1)
  st.SetPadTickY(1)

  st.SetPadColor(kWhite)

  # Margins:
  st.SetPadTopMargin(0.06)
  st.SetPadBottomMargin(0.12)
  st.SetPadLeftMargin(0.16)
  st.SetPadRightMargin(0.04)

  st.SetTitleFillColor(kWhite)
  st.SetTitleBorderSize(0)

  st.SetTitleOffset(1.1, "x")
  st.SetTitleOffset(1.6, "y")

  st.SetStatBorderSize(1)
  st.SetStatColor(0)

  st.SetLegendBorderSize(0)
  st.SetLegendFillColor(kWhite)
  st.SetLegendFont(st.GetLabelFont())
  # st.SetLegendTextSize( st.GetLabelSize() ) not in current ROOT version

  st.SetOptStat(0)

  # textSize = 0.05
  # st.SetLabelSize(textSize, "xyz")
  # st.SetTitleSize(textSize, "xyz")
  # st.SetLabelSize(textSize, "xyz")
  st.SetTitleSize(0.04, "xyz")

  st.SetTextFont(st.GetLabelFont())
  # st.SetTextSize(st.GetLabelSize())
  st.SetTextSize(0.05)

  # st.SetNdivisions(505, "xyz")
  st.SetNdivisions(510, "xyz")
  TGaxis.SetMaxDigits(4)

  st.SetTickLength(0.03, "XYZ")
  st.SetStripDecimals(kTRUE)
  st.SetLabelOffset(0.007, "XYZ")
  st.SetLegendTextSize(0.035)

  st.SetPalette(56)
  st.SetNumberContours(999)

  # st.SetErrorX(1)
  # st.SetErrorX(0)

  st.cd()
  return st

baseStyle()

def drawOpt(h, style):
  if style == "data":
    h.SetLineColor(kBlack)
    h.SetMarkerColor(kBlack)
    h.SetMarkerStyle(20)
    # h.SetMarkerSize(0.7)
    h.SetMarkerSize(1.)
    h.drawOption_ = "pz"
    if isinstance(h, TH1):
      h.SetBinErrorOption(TH1.kPoisson)
      # h.drawOption_="e0p0"
      h.drawOption_ = "e0e1p0"
      if integerContent(h):
        h.Sumw2(False)  # kPoisson uncertainties are drawn
  if style == "datalike":
    # h.SetLineColor(kBlack)
    # h.SetMarkerColor(kBlack)
    h.SetMarkerStyle(20)
    # h.SetMarkerSize(0.7)
    h.SetMarkerSize(0.8)
    # h.drawOption_ = "pze0"
    h.drawOption_ = "pz"
    if isinstance(h, TH1):
      h.drawOption_ = "e0e1p0"
  elif style == "pre":
    h.SetLineColor(kBlack)
    h.drawOption_ = "hist"
  elif style == "signal":
    # h.SetLineWidth(3)
    # h.SetLineWidth(2)
    h.SetLineWidth(2)
    # h.drawOption_ = "hist"
    h.drawOption_ = "hist"
  elif style == "signalWithErr":
    # h.SetLineWidth(0)
    # h.drawOption_ = "hist"
    h.SetMarkerStyle(0)
    h.SetMarkerSize(0)
    # h.SetLineColor(kGray)
    h.drawOption_ = "e2"
    # h.SetLineWidth(2)
    # h.drawOption_ = "e2 l"
    # h.drawOption_ = "e2 hist"
    # h.SetFillStyle(1001)
    h.SetFillStyle(3354)
    # h.SetFillColor(kGray)
  elif style == "signale":
    # h.SetLineWidth(3)
    h.SetLineWidth(2)
    # h.SetLineWidth(1)
    # h.drawOption_ = "hist"
    h.drawOption_ = "hist e0"
  elif style == "statUnc":
    # h.SetLineWidth(5)
    h.SetLineWidth(10)
    h.SetMarkerStyle(0)
    h.SetMarkerSize(0)
    # h.SetLineColor(kGray + 2)
    h.SetLineColor(kGray + 1)
    # h.SetLineColor(kGray)
    h.drawOption_ = "e2x0"
    # h.SetFillStyle(3254)
    h.SetFillStyle(0)
    h.SetFillColor(0)
    #h.drawOption_ = "e2"
  elif style == "statUncLikeTTH":
    # h.SetLineWidth(5)
    # h.SetLineWidth(2)
    # h.SetLineWidth(10)
    h.SetMarkerStyle(0)
    h.SetMarkerSize(0)
    h.SetLineColor(kGray+1)
    # h.SetLineColor(kGray)
    # h.drawOption_ = "e2x0"
    h.drawOption_ = "e2"
    # h.SetFillStyle(3254)
    h.SetFillStyle(1001)
    h.SetFillColor(kGray+1)
    #h.drawOption_ = "e2"
  elif style == "totErr":
    h.SetMarkerStyle(0)
    h.SetMarkerSize(0)
    h.SetLineColor(kGray)
    h.drawOption_ = "e2"
    h.SetFillStyle(1001)
    h.SetFillColor(kGray)
  elif style == "totUnc":
    # h.SetFillStyle(3254)
    h.SetFillStyle(3354)
    # h.SetFillStyle(3454)
    h.SetMarkerSize(0)
    # h.SetFillColor(kBlack)
    h.SetFillColor(kGray + 3)
    h.drawOption_ = "e2"
  elif style == "sysUnc":
    # h.SetFillStyle(3245)
    h.SetFillStyle(3345)
    # h.SetFillStyle(3445)
    h.SetMarkerSize(0)
    # h.SetFillColor(kRed)
    h.SetFillColor(kRed + 1)
    # gStyle.SetHatchesLineWidth(1.5)
    h.drawOption_ = "e2"
  elif style == "sysUncGraph":
    # h.SetFillStyle(3245)
    h.SetFillStyle(3445)
    # h.SetFillStyle(3001)
    # h.SetMarkerSize(0)
    h.SetFillColor(kRed)
    #h.drawOption_ = "a2 p same"
    h.drawOption_ = "a2same"
    #h.drawOption_ = "p"
  elif style == "sys":
    c = h.GetLineColor()
    h.SetFillColor(c)
    h.SetMarkerColor(c)
    h.SetFillStyle(3333)
    h.drawOption_ = "e2"
  else:
    print("Do not know what to do with draw option {}".format(style))

class Label:
  cmsEnergy = 13  # TeV

  def draw(self):
    varDict = vars(self)
    for varName, obj in varDict.iteritems():
      if isinstance(obj, TLatex):
        obj.SetNDC()
        obj.Draw()

  def __init__(self, drawAll=True, sim=False, status="", info="", year=None):
    if year=="2018":
      intLumi=intLumi18
    elif year=="2017":
      intLumi=intLumi17
    elif year=="2016APV":
      intLumi=intLumi16APV  
    elif year=="2016":
      intLumi=intLumi16
    elif year=="FR2":
      intLumi=intLumiFR2
    else:
      intLumi=0.
    saveStuff.append(self)
    # if status == "Private Work":
    if status == "WIP":
      if sim:
        # self.cms = TLatex( 0.2, .887, "#scale[0.76]{#font[52]{Private Work Simulation}}" )
        # self.cms = TLatex( 0.2, .95, "#scale[0.76]{#font[52]{Private Work Simulation}}" )
        # self.cms = TLatex( 0.2, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress Simulation}}" )
        # self.cms = TLatex( 0.15, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress Simulation}}" )
        self.cms = TLatex(
          # 0.16, .92, "#splitline{#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}}{#scale[0.76]{#font[52]{  Simulation}}}")
          # 0.16, .95, "#splitline{#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}}{#scale[0.76]{#font[52]{  Simulation}}}")
          # 0.16, .92, "#splitline{#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}}{#scale[0.76]{#font[52]{  Simulation}}}")
          0.18, .85, "#splitline{#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}}{#scale[0.76]{#font[52]{  Simulation}}}")
      else:
        # self.pub = TLatex( 0.2, .887, "#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}")
        # self.pub = TLatex(
        #     0.15, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}")
        self.pub = TLatex(
          # 0.28, .95, "#font[61]{CMS} #scale[0.68]{#font[52]{Work in Progress}}")
          # 0.16, .95, "#font[61]{CMS} #scale[0.68]{#font[52]{Work in Progress}}")
          # 0.18, .85, "#font[61]{CMS} #scale[0.68]{#font[52]{Work in Progress}}")
          0.18, .87, "#font[61]{CMS} #scale[0.68]{#font[52]{Work in Progress}}")
    else:
      if sim:
        self.cms = TLatex(
          # 0.16, .948, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .948, "#scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .95, "#scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.18, .87, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.18, .85, "#splitline{#font[61]{CMS}}{#scale[0.76]{#font[52]{  Simulation}}}")
          0.19, .85, "#splitline{#font[61]{CMS}}{#scale[0.76]{#font[52]{  Simulation}}}")
        # self.sim = TLatex(
        #     0.16, 0.902, "#scale[0.76]{#font[52]{  Simulation}}")
        # 0.2, .887, "#font[61]{CMS} #scale[0.76]{#font[52]{Simulation}}")
      else:
        # self.cms = TLatex(0.2, .887, "#font[61]{CMS}")
        self.cms = TLatex(
          # 0.16, .948, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .948, "#scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .95, "#scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.18, .87, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          0.19, .87, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
      # self.pub = TLatex(
      #     0.2, .857, "#scale[0.76]{#font[52]{%s}}" % status)
    if year:
      if year=="FR2":
        # self.lum = TLatex(.62, .95,
        #                        "%.1f fb^{-1} (%s TeV)" % (np.round(intLumi / 1000., 0), self.cmsEnergy))
        # self.lum = TLatex(.71, .95,"#scale[0.72]{%.1f fb^{-1} (%s TeV)}" % (np.round(intLumi / 1000., 0), self.cmsEnergy))
        lText = '{:.0f}'.format(intLumi / 1000.)
        self.lum = TLatex(.69, .95,"#scale[0.72]{%.0f fb^{-1} (%s TeV)}" % (intLumi / 1000., self.cmsEnergy))
      else:
        # self.lum = TLatex(.62, .95, "%.1f fb^{-1} (%s TeV)" % (intLumi / 1000., self.cmsEnergy))
        # self.lum = TLatex(.67, .95, "#scale[0.72]{%.1f fb^{-1} (%s TeV)}" % (intLumi / 1000., self.cmsEnergy))
        self.lum = TLatex(.71, .95, "#scale[0.72]{%.1f fb^{-1} (%s TeV)}" % (intLumi / 1000., self.cmsEnergy))
    if info:
      # self.info = TLatex(.15, .95, info)
      self.info = TLatex(.16, .95, info)
    #if info: self.info = TLatex( .85, .85, info )

    if drawAll:
      self.draw()

def save(name, folder="plots/", endings=[".pdf"], normal=False, log=True, changeMinMax=True):
  name = modifySaveName(name)
  if normal:
    for ending in endings:
      gPad.GetCanvas().SaveAs(folder + name + ending)
  if log:
    allH2s = [i for i in gPad.GetCanvas().GetListOfPrimitives() if isinstance(i, TH2)]
    if allH2s:
      gPad.GetCanvas().SetLogz()
    else:
      if changeMinMax:
        setMinMaxForLog()
      gPad.GetCanvas().SetLogy(True)
    for ending in endings:
      gPad.GetCanvas().SaveAs(folder + name + "_log" + ending)
  gPad.GetCanvas().SetLogy(False)

def get_hvalues(infile, histos, hdict, mtdict, name = "MTop"):
  """
  input: fit diagnostic file from 'combine -M MultiDimFit' with '--saveWorkspace --saveFitResult'
  """
  if not os.path.exists(infile):
    return False

  with open(infile, 'r') as f:
    data = json.load(f)

    if len(data['POIs']) != 1:
      return False
    
    if data['POIs'][0]['name'] != name:
      return False

    topDat = data['POIs'][0]['fit']
    #if np.abs(hai) > 0.00000001 and np.abs(lou) > 0.00000001:
    histos[0].Fill(topDat[1])
    histos[1].Fill(topDat[2])
    histos[2].Fill(topDat[0])
    histos[3].Fill(topDat[1])
    histos[4].Fill(topDat[2])
    histos[5].Fill(topDat[0])

    # if list of params is given, directly search for these params e.g. ['rT', 'a']
    for param in data['params']:
      cName = param['name']
      if cName in hdict:
        parDat = param['fit']
        hs = hdict[cName]
        hs[0].Fill(parDat[1])
        hs[1].Fill(parDat[2])
        hs[2].Fill(parDat[0])
        hs[3].Fill(parDat[1])
        hs[4].Fill(parDat[2])
        hs[5].Fill(parDat[0])
        if cName in mtdict:
          mtDat = param[name]
          mts = mtdict[cName]
          mts[0].Fill(mtDat[2] - mtDat[1])
          mts[1].Fill(mtDat[0] - mtDat[1])
      # else:
      #   print("Parameter {} no available in histos!".format(param['name']))

  return True

def get_values(input, symbol="MTop", params=None):
  """
  input: fit diagnostic file from 'combine -M MultiDimFit' with '--saveWorkspace --saveFitResult'
  """
  fitDiagnosticsFile = TFile(input, "READ")
  fitResult = fitDiagnosticsFile.Get("fit_mdf")

  vals = []
  errHi = []
  errLo = []
  err = []

  # if list of params is given, directly search for these params e.g. ['rT', 'a']
  if params:
    for p in params:
      var = fitResult.floatParsFinal().find(p)
      if var != None:
        vals.append(var.getValV())
        errLo.append(var.getErrorLo())
        errHi.append(var.getErrorHi())
        err.append(var.getError())
  else:
    # otherwise look for symbol correlation matrix
    i = 0
    while True:
      var = fitResult.floatParsFinal().find(symbol+str(i))
      if var == None:
        break;

      vals.append(var.getValV())
      errLo.append(var.getErrorLo())
      errHi.append(var.getErrorHi())
      err.append(var.getError())

      i+=1

  fitDiagnosticsFile.Close()
  return np.array(vals), np.array(errLo), np.array(errHi), np.array(err)

def get_correlation_matrix(input, numberOfGenBins=4, symbol="r", params=None):
  """
  input: multidim fit file from 'combine -M MultiDimFit' with '--saveWorkspace --saveFitResult'
  """
  fitDiagnosticsFile = TFile(input, "READ")
  fitResult = fitDiagnosticsFile.Get("fit_mdf")

  if params:
    corrMat = np.empty((len(params),len(params)))
    for i, p1 in enumerate(params):
      for j, p2 in enumerate(params):
        corrMat[i][j] = fitResult.correlation(p1,p2)
  else:
    corrMat = np.empty((numberOfGenBins,numberOfGenBins))

    # correlation matrix
    for i in range(numberOfGenBins):
      for j in range(numberOfGenBins):
        corrMat[i][j] = fitResult.correlation(symbol+str(i),symbol+str(j))

  return corrMat

def get_covariance_matrix(corrMat, errors, numberOfGenBins=4):
  # covariance matrix
  covMat = np.empty((numberOfGenBins,numberOfGenBins))
  for i in range(numberOfGenBins):
    for j in range(numberOfGenBins):
      covMat[i][j] = corrMat[i][j]
      covMat[i][j] *= errors[i]
      covMat[i][j] *= errors[j]

  return covMat

def main():
  ### args
  parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument('-o', '--output', dest='output', action='store', default="ToyPlots", help='path to output directory')
  parser.add_argument('-i', '--input', dest='input', action='store', required=True, help='shape of where to generate toys from')
  if len(sys.argv)==1:
    print ("ERROR: Not enough arguments provided!")
    parser.print_help()
    parser.exit()
  opts, opts_unknown = parser.parse_known_args()

  outFolder = opts.output + "/Plots/"
  if not os.path.exists(outFolder):
    os.makedirs(outFolder)

  h_mass = TH1F("MTop", "", 400, -0.1, 0.1)
  h_mass_errUp = TH1F("MTop_errUp", "", 300, 0, 0.3)
  h_mass_errDn = TH1F("MTop_errDn", "", 300, -0.3, 0.)
  h_mass2 = TH1F("MTop2", "", 600, -0.3, 0.3)
  h_mass2_errUp = TH1F("MTop2_errUp", "", 600, -0.3, 0.3)
  h_mass2_errDn = TH1F("MTop2_errDn", "", 600, -0.3, 0.3)
  histos = [h_mass, h_mass_errUp, h_mass_errDn, h_mass2, h_mass2_errUp, h_mass2_errDn]

  histosNuis = {}
  histosNuisMt = {}

  toysLatex = TLatex()
  toysLatex.SetTextSize(0.65 * toysLatex.GetTextSize())

  for nuis in nuisances:
    histosNuis[nuis] = []
    histosNuis[nuis].append(TH1F(nuis, "", 400, -0.2, 0.2))
    histosNuis[nuis][-1].SetDirectory(0)
    histosNuis[nuis].append(TH1F(nuis+"_up", "", 210, 0., 1.05))
    histosNuis[nuis][-1].SetDirectory(0)
    histosNuis[nuis].append(TH1F(nuis+"_dn", "", 210, -1.05, 0.))
    histosNuis[nuis][-1].SetDirectory(0)
    histosNuis[nuis].append(TH1F(nuis + "_common", "", 420, -1.05, 1.05))
    histosNuis[nuis][-1].SetDirectory(0)
    histosNuis[nuis].append(TH1F(nuis + "_commonup", "", 420, -1.05, 1.05))
    histosNuis[nuis][-1].SetDirectory(0)
    histosNuis[nuis].append(TH1F(nuis + "_commondn", "", 420, -1.05, 1.05))
    histosNuis[nuis][-1].SetDirectory(0)
    histosNuisMt[nuis] = []
    histosNuisMt[nuis].append(TH1F(nuis+"_MTup", "", 300, -0.15, 0.15))
    histosNuisMt[nuis][-1].SetDirectory(0)
    histosNuisMt[nuis].append(TH1F(nuis+"_MTdn", "", 300, -0.15, 0.15))
    histosNuisMt[nuis][-1].SetDirectory(0)

  #collect inputs
  fileList = glob.glob(opts.input + "/impacts*json")
  counter = 0
  for filemon in fileList:
    if get_hvalues(filemon, histos, histosNuis, histosNuisMt):
      counter += 1
      if counter%100 == 0:
        print("Processed {}".format(counter))
      #corrMat = get_correlation_matrix(file, nPOIs, "MTop")
      #covMat = get_covariance_matrix(corrMat, err_total, len(err_total))

  can = TCanvas()
  for direction in ["bestfit", "dn", "up"]:
    can.Clear()
    if direction == "bestfit":
      h = histos[0]
    elif direction == "up":
      h = histos[1]
    elif direction == "dn":
      h = histos[2]
    h.GetYaxis().SetTitle("Toy Experiments")
    h.GetXaxis().SetTitle("Post-fit " + nuis + ("" if direction == "bestfit" else " " + direction))
    h.SetLineWidth(2)
    h.SetLineColor(kBlue+1)

    h.SetMaximum(1.7 * h.GetMaximum())
    h.Draw("hist 0")
    leg = TLegend(.65, .75, .95, .92)
    leg.SetFillColor(kWhite)
    leg.SetFillStyle(0)
    leg.AddEntry(h, "Fits to Asimov", "l")
    leg.Draw("SAME")

    info = ""
    toysLatex.DrawLatexNDC(0.23, 0.75, "Toys: "+str(int(h.GetEntries())))
    toysLatex.DrawLatexNDC(0.23, 0.70, "Mean: "+str(np.round(h.GetMean(),5)))
    toysLatex.DrawLatexNDC(0.23, 0.65, "RMS: "+str(np.round(h.GetRMS(),5)))
    l = Label(status="WIP",info="#scale[0.7]{%s}" % info, sim=False, year="")
    saveName = "MTop_"+direction
    gPad.RedrawAxis()
    save(saveName, folder=outFolder, normal=True, log=False)
  
  can.Clear()
  hColl = histos[3]
  hUp = histos[4]
  hDn = histos[5]
  hColl.GetYaxis().SetTitle("Toy Experiments")
  hColl.GetXaxis().SetTitle("Post-fit m_{t} [GeV]")
  hColl.SetLineWidth(2)
  hColl.SetLineColor(kGreen + 2)
  hUp.SetLineColor(kRed)
  hDn.SetLineColor(kBlue)

  # Draw
  hColl.SetMaximum(1.9 * max(hColl.GetMaximum(),hUp.GetMaximum()))
  hColl.Draw("hist 0")
  #hUp.Draw("hist 0 SAME")
  #hDn.Draw("hist 0 SAME")
  leg = TLegend(.65, .75, .95, .92)
  leg.SetFillColor(kWhite)
  leg.SetFillStyle(0)
  leg.AddEntry(hColl, "nominal", "l")
  #leg.AddEntry(hUp, "+#sigma uncertainty", "l")
  #leg.AddEntry(hDn, "-#sigma uncertainty", "l")
  leg.Draw("SAME")
  info = ""
  toysLatex.DrawLatexNDC(0.2, 0.80, "Toys: " + str(int(int(np.round(hColl.GetEntries())))))
  toysLatex.DrawLatexNDC(0.2, 0.75, "Nominal:                 " + str(np.round(histos[0].GetMean(),4)) + " #pm " + str(np.round(histos[0].GetRMS(),4)))
  #toysLatex.DrawLatexNDC(0.2, 0.70, "Uncertainty up:       " + str(np.round(histos[1].GetMean(),4)) + " #pm " + str(np.round(histos[1].GetRMS(),4)))
  #toysLatex.DrawLatexNDC(0.2, 0.65, "Uncertainty down: " + str(np.round(histos[2].GetMean(),4)) + " #pm " + str(np.round(histos[2].GetRMS(),4)))
  l = Label(status="WIP",info="#scale[0.7]{%s}" % info, sim=False, year="")
  saveName = "MTop_All"
  gPad.RedrawAxis()
  save(saveName, folder=outFolder, normal=True, log=False)

  for iNuis in range(len(nuisances)):
    nuis = nuisances[iNuis]
    for idir,direction in enumerate(["bestfit","up","dn"]):
      can.Clear()
      h = histosNuis[nuis][idir]
      h.GetYaxis().SetTitle("Toy Experiments")
      h.GetXaxis().SetTitle("Post-fit " + nuis + ("" if idir == 0 else " " + direction))
      h.SetLineWidth(2)
      h.SetLineColor(kBlue+1)

      h.SetMaximum(1.7 * h.GetMaximum())
      h.Draw("hist 0")
      leg = TLegend(.65, .75, .95, .92)
      leg.SetFillColor(kWhite)
      leg.SetFillStyle(0)
      leg.AddEntry(h, "Fits to Asimov", "l")
      leg.Draw("SAME")

      info = ""
      toysLatex.DrawLatexNDC(0.23, 0.80, "Toys: "+str(int(h.GetEntries())))
      toysLatex.DrawLatexNDC(0.23, 0.75, "Mean: "+str(np.round(h.GetMean(),5)))
      toysLatex.DrawLatexNDC(0.23, 0.70, "RMS: "+str(np.round(h.GetRMS(),5)))
      l = Label(status="WIP",info="#scale[0.7]{%s}" % info, sim=False, year="")
      saveName = nuis + "_" + direction
      gPad.RedrawAxis()
      save(saveName, folder=outFolder, normal=True, log=False)

    can.Clear()
    hColl = histosNuis[nuis][3]
    hUp = histosNuis[nuis][4]
    hDn = histosNuis[nuis][5]
    hColl.GetYaxis().SetTitle("Toy Experiments")
    hColl.GetXaxis().SetTitle("Post-fit " + nuis)
    hColl.SetLineWidth(2)
    hColl.SetLineColor(kGreen + 2)
    hUp.SetLineColor(kRed)
    if nuis not in singleside:
      hDn.SetLineColor(kBlue)

    hColl.SetMaximum(1.9 * max(hColl.GetMaximum(), hUp.GetMaximum()))
    hColl.Draw("hist 0")
    hUp.Draw("hist 0 SAME")
    if nuis not in singleside:
      hDn.Draw("hist 0 SAME")
    leg = TLegend(.65, .75, .95, .92)
    leg.SetFillColor(kWhite)
    leg.SetFillStyle(0)
    leg.AddEntry(hColl, "nominal", "l")
    leg.AddEntry(hUp, "+#sigma variation", "l")
    if nuis not in singleside:
      leg.AddEntry(hDn, "-#sigma variation", "l")
    leg.Draw("SAME")

    info = ""
    toysLatex.DrawLatexNDC(0.2, 0.80, "Toys: " + str(int(hColl.GetEntries())))
    toysLatex.DrawLatexNDC(0.2, 0.75, "Nominal:               " + str(np.round(histosNuis[nuis][0].GetMean(),4)) + " #pm " + str(np.round(histosNuis[nuis][0].GetRMS(),4)))
    toysLatex.DrawLatexNDC(0.2, 0.70, "Constraint up:       " + str(np.round(histosNuis[nuis][1].GetMean(),4)) + " #pm " + str(np.round(histosNuis[nuis][1].GetRMS(),4)))
    if nuis not in singleside:
      toysLatex.DrawLatexNDC(0.2, 0.65, "Constraint down: " + str(np.round(histosNuis[nuis][2].GetMean(),4)) + " #pm " + str(np.round(histosNuis[nuis][2].GetRMS(),4)))
    l = Label(status="WIP",info="#scale[0.7]{%s}" % info, sim=False, year="")
    saveName = nuis + "_All"
    gPad.RedrawAxis()
    save(saveName, folder=outFolder, normal=True, log=False)

    for idir,direction in enumerate(["up","dn"]):
      can.Clear()
      h = histosNuisMt[nuis][idir]
      h.GetYaxis().SetTitle("Toy Experiments")
      h.GetXaxis().SetTitle("{} post-fit impact on {} [GeV]".format(nuis + ("" if idir == 0 else " " + direction),"m_{t}"))
      h
      h.SetLineWidth(2)
      h.SetLineColor(kBlue+1)

      h.SetMaximum(1.7 * h.GetMaximum())
      h.Draw("hist 0")
      leg = TLegend(.65, .75, .95, .92)
      leg.SetFillColor(kWhite)
      leg.SetFillStyle(0)
      leg.AddEntry(h, "Fits to Asimov", "l")
      leg.Draw("SAME")

      info = ""
      toysLatex.DrawLatexNDC(0.23, 0.75, "entries: "+str(int(h.GetEntries())))
      toysLatex.DrawLatexNDC(0.23, 0.70, "mean: "+str(np.round(h.GetMean(),5)))
      toysLatex.DrawLatexNDC(0.23, 0.65, "rms: "+str(np.round(h.GetRMS(),5)))
      l = Label(status="WIP",info="#scale[0.7]{%s}" % info, sim=False, year="")
      saveName = nuis + "_MT" + direction
      gPad.RedrawAxis()
      save(saveName, folder=outFolder, normal=True, log=False)

    can.Clear()
    # hColl = histos[3]
    hUp = histosNuisMt[nuis][0]
    if nuis not in singleside:
      hDn = histosNuisMt[nuis][1]
    hUp.GetYaxis().SetTitle("Toy Experiments")
    hUp.GetXaxis().SetTitle("{} post-fit impact on {} [GeV]".format(nuis,"m_{t}"))
    #hColl.SetLineColor(kGreen + 2)
    hUp.SetLineColor(kRed)
    if nuis not in singleside:
      hDn.SetLineColor(kBlue)

    hUp.Draw("hist 0")
    # hColl.Draw("SAME")
    if nuis not in singleside:
      hDn.Draw("hist 0 SAME")
    leg = TLegend(.65, .75, .95, .92)
    leg.SetFillColor(kWhite)
    leg.SetFillStyle(0)
    #leg.AddEntry(hColl, "nominal", "l")
    leg.AddEntry(hUp, "+#sigma variation", "l")
    if nuis not in singleside:
      leg.AddEntry(hDn, "-#sigma variation", "l")
    leg.Draw("SAME")

    info = ""
    toysLatex.DrawLatexNDC(0.2, 0.80, "Toys: " + str(int(hUp.GetEntries())))
    #toysLatex.DrawLatexNDC(0.2, 0.75, "Nominal:              " + str(np.round(histosNuis[nuis][0].GetMean(),4)) + " #pm " + str(np.round(histosNuis[nuis][0].GetRMS(),4)))
    toysLatex.DrawLatexNDC(0.2, 0.75, "m_{t} impact up:     " + str(np.round(hUp.GetMean(),4)) + " #pm " + str(np.round(histosNuisMt[nuis][0].GetRMS(),4)))
    if nuis not in singleside:
      toysLatex.DrawLatexNDC(0.2, 0.70, "m_{t} impact down: " + str(np.round(hDn.GetMean(),4)) + " #pm " + str(np.round(histosNuisMt[nuis][1].GetRMS(),4)))
    l = Label(status="WIP",info="#scale[0.7]{%s}" % info, sim=False, year="")
    saveName = nuis + "_MT_All"
    gPad.RedrawAxis()
    save(saveName, folder=outFolder, normal=True, log=False)

  # can.Clear()
  # s = style.style2d()
  # h2_r0_r1.GetXaxis().SetTitle("Fitted r0")
  # h2_r0_r1.GetYaxis().SetTitle("Fitted r1")
  # gStyle.SetPalette(kThermometer)
  # h2_r0_r1.Draw("col")
  # info = ""
  # nEntries = h2_r0_r1.GetEntries()
  # entriesLatex = TLatex()
  # entriesLatex.SetTextSize(0.65 * entriesLatex.GetTextSize())
  # entriesLatex.DrawLatexNDC(0.23, 0.75, "entries: "+str(int(nEntries)))
  # corrLatex = TLatex()
  # corrLatex.SetTextSize(0.65 * corrLatex.GetTextSize())
  # corrLatex.DrawLatexNDC(0.23, 0.70, "correlation: "+str(np.round(h2_r0_r1.GetCorrelationFactor(),6)))
  # l = Label(info="#scale[0.7]{%s}" % info, sim=False, year="")
  # saveName = "ToyStudy_r0_r1"
  # save(saveName, folder=outFolder+"/correlations/", normal=True, log=False)

if __name__ == "__main__":
  main()
