#!/bin/bash

#inloc=/nfs/dust/cms/user/hsiikone/.pseudoexp/correct/
inloc=/afs/desy.de/user/m/mmyllyma/Work/CombineAnalysis/CMSSW_10_2_13/src/Datacards/ToyCreation/output
outloc=./

python plotToyJSONs.py -i ${inloc} -o ${outloc}
