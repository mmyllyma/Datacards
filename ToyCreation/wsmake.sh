#!/bin/bash

Folders="5D"
#Folders="full half 1D 2D 2Dfull 2Dhalf 3D 3Db 4D 4Dfull 4Dhalf 5D 5Db"
#Leps="ele"
#Leps="muo"
Leps="ele muo"
#Years="2017"
Years="2018"
#Years="2016"

#outloc=/nfs/dust/cms/user/mmyllyma/.Toys/workspace
outloc=/afs/desy.de/user/m/mmyllyma/Work/CombineAnalysis/CMSSW_10_2_13/src/Datacards/ToyCreation/workspace
NToys=1000
#Offset=4000
Offset=0

for IBase in $(seq 0 $NToys); do
  IToy=$(($IBase+$Offset))
  python createToyHistos.py -t $IToy -c $Leps -y $Years -f $Folders -o ${outloc} -i ../
  wait
  mv ${outloc}/${IToy}/work/space.root ${outloc}/space_${IToy}.root
  wait
  for fol in $Folders; do
    rm ${outloc}/${IToy}/${fol}/*.root
    rm ${outloc}/${IToy}/work/card.txt
    wait
    rmdir ${outloc}/${IToy}/${fol}
    rmdir ${outloc}/${IToy}/work
    wait
    rmdir ${outloc}/${IToy}
  done
  wait
done
