#!/usr/bin/env python
from ROOT import *
from array import array
import numpy as np
from scipy import stats
import argparse
import glob
import sys

#ROOT.PyConfig.IgnoreCommandLineOptions = False
gROOT.gPrintViaErrorHandler = True
gROOT.SetBatch(True)

import array
import collections
import math
import os.path
import optparse
import re
import subprocess

# private libs
import uncertainties as unc

divideByBinWidth = False
saveStuff = []

def addYears(target, yrs, added):
  for add in added:
    for yr in yrs:
      target.append(add + "_" + yr)

years = ["2016APV", "2016", "2017", "2018"]
nuisances = []
# Muon-only
#  ["MuoIDSF", "MuoIsoSF", "MuoIDSFStat", "MuoIsoSFStat", "MuoTrgSF"]
#nuisances += ["MuoScaleStat", "MuoScaleEWK", "MuoScaleEWK2", "MuoScaleZPt", "MuoScaleDM"]
# Electron-only
#nuisances += ["EleIDSF", "EleRecoSF", "EleTrgSF", "EleScale", "EleReso"]
nuisances = []
# FSR
nuisances += ["FSRx2xg", "FSRq2qg", "FSRg2gg", "FSRg2qq", "FSRCNSx2xg", "FSRCNSq2qg", "FSRCNSg2gg", "FSRCNSg2qq"]
# ISR
nuisances += ["ISR", "ISRst"]
# B-fragmentation
nuisances += ["BFragPeterson", "BFragBL", "BFragSL"]
# B-tagging
nuisances += ["BCTagSF_corr", "BMisTagSF_corr"]
addYears(nuisances, years, ["BCTagSF", "BMisTagSF"])
# Misc
nuisances += ["L1Prefire", "FactScale", "RenScale", "Pileup", "TopPt"]
# PDF
nuisances += ["AlphaS", "PDF", "CT14", "MMHT2014"]
# Flavor JEC
nuisances += ["FlavorPureBottom", "FlavorPureCharm", "FlavorPureQuark", "FlavorPureGluon"]
# 100% correlated JEC
nuisances += ["JECAbsoluteMPFBias", "JECAbsoluteScale", "JECFragmentation", "JECSinglePionECAL", "JECSinglePionHCAL"]
# 50% correlated JEC
nuisances += ["JECPileUpDataMC", "JECPileUpPtBB", "JECPileUpPtEC1", "JECPileUpPtEC2", "JECPileUpPtRef", "JECPileUptPtHF", "JECRelativeFSR", "JECRelativeJERHF", "JECRelativePtBB", "JECRelativePtHF", "JECRelativeBal"]
# 0% correlated JEC
addYears(nuisances, years, ["JECAbsoluteStat", "JECRelativeJEREC1", "JECRelativeJEREC2", "JECRelativePtEC1", "JECRelativePtEC2", "JECRelativeSample", "JECRelativeStatEC", "JECRelativeStatFSR", "JECRelativeStatHF", "JECTimePtEta"])
# JER/MET
addYears(nuisances, years, ["JERInner", "JERMid", "JEROuter", "MET"])
# Two-sided troublemakers
nuisances += ["HDAMP", "UETune"]
# One-sided troublemakers
nuisances += ["CRQCDBased", "CRGluonMove", "ERD"]

def dataLikeName(name):
  return "Data" in name or "Pseudodata" in name or "Direct" in name

def modifySaveName(name):
  replacements = {"(": "", ")": "", "&": "AND", ".": "p",
                  "/": "DIV", "<": "", ">": "", "*": "TIMES", "$": "DOLLAR"}
  for a, b in replacements.iteritems():
    name = name.replace(a, b)
  return name

def integerContent(h, scaledByWidth=False):
  for bin in loopH(h):
    c = h.GetBinContent(bin)
    if scaledByWidth:
      c *= h.GetBinWidth(bin)
    if abs(c - int(round(c))) > 1e-5:
      return False
  return True

def getPoissonUnc(n):
  # http://prd.aps.org/abstract/PRD/v86/i1/e010001 (p.399)

  # calculate x = 1-alpha ( approx 68% )
  x = TMath.Erf(1. / sqrt(2))
  alpha = 1 - x

  # for central confidence intervals, alpha_lo =alpha_up = alpha/2
  alpha_lo = alpha / 2
  alpha_up = alpha / 2

  # confidence interval is [ xlo, xup ]
  xlo = 0.5 * TMath.ChisquareQuantile(alpha_lo, 2 * n)
  xup = 0.5 * TMath.ChisquareQuantile(1 - alpha_up, 2 * (n + 1))
  return n - xlo, xup - n

def baseStyle():
  st = TStyle("defaultStyle", "Gangnam style")
  st.SetCanvasColor(kWhite)
  st.SetCanvasBorderMode(0)
  st.SetFrameBorderMode(0)
  st.SetCanvasDefH(800)
  st.SetCanvasDefW(800)

  st.SetPadTickX(1)
  st.SetPadTickY(1)

  st.SetPadColor(kWhite)

  # Margins:
  st.SetPadTopMargin(0.06)
  st.SetPadBottomMargin(0.12)
  st.SetPadLeftMargin(0.16)
  st.SetPadRightMargin(0.04)

  st.SetTitleFillColor(kWhite)
  st.SetTitleBorderSize(0)

  st.SetTitleOffset(1.1, "x")
  st.SetTitleOffset(1.6, "y")

  st.SetStatBorderSize(1)
  st.SetStatColor(0)

  st.SetLegendBorderSize(0)
  st.SetLegendFillColor(kWhite)
  st.SetLegendFont(st.GetLabelFont())
  # st.SetLegendTextSize( st.GetLabelSize() ) not in current ROOT version

  st.SetOptStat(0)

  # textSize = 0.05
  # st.SetLabelSize(textSize, "xyz")
  # st.SetTitleSize(textSize, "xyz")
  # st.SetLabelSize(textSize, "xyz")
  st.SetTitleSize(0.04, "xyz")

  st.SetTextFont(st.GetLabelFont())
  # st.SetTextSize(st.GetLabelSize())
  st.SetTextSize(0.05)

  # st.SetNdivisions(505, "xyz")
  st.SetNdivisions(510, "xyz")
  TGaxis.SetMaxDigits(4)

  st.SetTickLength(0.03, "XYZ")
  st.SetStripDecimals(kTRUE)
  st.SetLabelOffset(0.007, "XYZ")
  # st.SetLegendTextSize(0.02)

  st.SetPalette(56)
  st.SetNumberContours(999)

  # st.SetErrorX(1)
  # st.SetErrorX(0)

  st.cd()
  return st

baseStyle()

def drawOpt(h, style):
  if style == "data":
    h.SetLineColor(kBlack)
    h.SetMarkerColor(kBlack)
    h.SetMarkerStyle(20)
    # h.SetMarkerSize(0.7)
    h.SetMarkerSize(1.)
    h.drawOption_ = "pz"
    if isinstance(h, TH1):
      h.SetBinErrorOption(TH1.kPoisson)
      # h.drawOption_="e0p0"
      h.drawOption_ = "e0e1p0"
      if integerContent(h):
        h.Sumw2(False)  # kPoisson uncertainties are drawn
  if style == "datalike":
    # h.SetLineColor(kBlack)
    # h.SetMarkerColor(kBlack)
    h.SetMarkerStyle(20)
    # h.SetMarkerSize(0.7)
    h.SetMarkerSize(0.8)
    # h.drawOption_ = "pze0"
    h.drawOption_ = "pz"
    if isinstance(h, TH1):
      h.drawOption_ = "e0e1p0"
  elif style == "pre":
    h.SetLineColor(kBlack)
    h.drawOption_ = "hist"
  elif style == "signal":
    # h.SetLineWidth(3)
    # h.SetLineWidth(2)
    h.SetLineWidth(2)
    # h.drawOption_ = "hist"
    h.drawOption_ = "hist"
  elif style == "signalWithErr":
    # h.SetLineWidth(0)
    # h.drawOption_ = "hist"
    h.SetMarkerStyle(0)
    h.SetMarkerSize(0)
    # h.SetLineColor(kGray)
    h.drawOption_ = "e2"
    # h.SetLineWidth(2)
    # h.drawOption_ = "e2 l"
    # h.drawOption_ = "e2 hist"
    # h.SetFillStyle(1001)
    h.SetFillStyle(3354)
    # h.SetFillColor(kGray)
  elif style == "signale":
    # h.SetLineWidth(3)
    h.SetLineWidth(2)
    # h.SetLineWidth(1)
    # h.drawOption_ = "hist"
    h.drawOption_ = "hist e0"
  elif style == "statUnc":
    # h.SetLineWidth(5)
    h.SetLineWidth(10)
    h.SetMarkerStyle(0)
    h.SetMarkerSize(0)
    # h.SetLineColor(kGray + 2)
    h.SetLineColor(kGray + 1)
    # h.SetLineColor(kGray)
    h.drawOption_ = "e2x0"
    # h.SetFillStyle(3254)
    h.SetFillStyle(0)
    h.SetFillColor(0)
    #h.drawOption_ = "e2"
  elif style == "statUncLikeTTH":
    # h.SetLineWidth(5)
    # h.SetLineWidth(2)
    # h.SetLineWidth(10)
    h.SetMarkerStyle(0)
    h.SetMarkerSize(0)
    h.SetLineColor(kGray+1)
    # h.SetLineColor(kGray)
    # h.drawOption_ = "e2x0"
    h.drawOption_ = "e2"
    # h.SetFillStyle(3254)
    h.SetFillStyle(1001)
    h.SetFillColor(kGray+1)
    #h.drawOption_ = "e2"
  elif style == "totErr":
    h.SetMarkerStyle(0)
    h.SetMarkerSize(0)
    h.SetLineColor(kGray)
    h.drawOption_ = "e2"
    h.SetFillStyle(1001)
    h.SetFillColor(kGray)
  elif style == "totUnc":
    # h.SetFillStyle(3254)
    h.SetFillStyle(3354)
    # h.SetFillStyle(3454)
    h.SetMarkerSize(0)
    # h.SetFillColor(kBlack)
    h.SetFillColor(kGray + 3)
    h.drawOption_ = "e2"
  elif style == "sysUnc":
    # h.SetFillStyle(3245)
    h.SetFillStyle(3345)
    # h.SetFillStyle(3445)
    h.SetMarkerSize(0)
    # h.SetFillColor(kRed)
    h.SetFillColor(kRed + 1)
    # gStyle.SetHatchesLineWidth(1.5)
    h.drawOption_ = "e2"
  elif style == "sysUncGraph":
    # h.SetFillStyle(3245)
    h.SetFillStyle(3445)
    # h.SetFillStyle(3001)
    # h.SetMarkerSize(0)
    h.SetFillColor(kRed)
    #h.drawOption_ = "a2 p same"
    h.drawOption_ = "a2same"
    #h.drawOption_ = "p"
  elif style == "sys":
    c = h.GetLineColor()
    h.SetFillColor(c)
    h.SetMarkerColor(c)
    h.SetFillStyle(3333)
    h.drawOption_ = "e2"
  else:
    print "Do not know what to do with draw option", style

class Multiplot:
  def __init__(self, legCoords=None):
    self.hists = []
    self.histsToStack = []

    # todo: impmlement setter and getter
    self.minimum = None
    self.maximum = None

    #self.leg = TLegend(.56,.59,.94,.915)
    # self.leg = TLegend(.56, .69, .94, .915)
    if legCoords:
      self.leg = TLegend(legCoords[0],legCoords[1],legCoords[2],legCoords[3])
    else:
      # self.leg = TLegend(.52, .69, .94, .915)
      self.leg = TLegend(.72, .69, .94, .915)
    self.leg.SetFillColor(kWhite)
    self.leg.SetFillStyle(0)

  def add(self, h, label=""):
    h.SetName(label)
    self.hists.append(h)

  def addStack(self, h, label=""):
    h.SetName(label)
    self.histsToStack.append(h)

  def getMinimum(self):
    return min([h.GetMinimum(0) for h in self.hists + self.histsToStack if not isinstance(h, THStack) and not isinstance(h, TGraph)])

  def getMaximum(self):
    return max([h.GetMaximum() for h in self.hists])

  def getStack(self):
    stacks = [h for h in self.hists if isinstance(h, THStack)]
    return stacks[0] if stacks else None

  def stackHists(self):
    if not self.histsToStack:
      return
    stack = THStack()
    stack.SetTitle(";%s;%s" % (self.histsToStack[0].GetXaxis().GetTitle(), self.histsToStack[0].GetYaxis().GetTitle()))
    stack.drawOption_ = "hist"
    #stack.drawOption_ = "hist e2"
    for h in self.histsToStack:
      h.SetFillColor(h.GetLineColor())
      h.SetLineColor(kBlack)
      stack.Add(h)

    self.hists.append(stack)

  def sortStackByIntegral(self):
    self.histsToStack = sorted(self.histsToStack, key=lambda x: x.Integral(0, -1))

  def Draw(self, legColumns=1):
    if not self.hists and not self.histsToStack:
      return False
    self.stackHists()

    #minimum = 1e-5
    #minimum = 1e-3
    minimum = self.getMinimum()
    # minimum = 0.1
    #minimum = 0.001
    # maximum = 1.5*self.getMaximum()
    maximum = 1.75*self.getMaximum()
    # maximum = 10.1 * self.getMaximum()
    #maximum = 1.1*self.getMaximum()*100.
    #maximum = 0.1

    if self.maximum != None:
      maximum = self.maximum
    if self.minimum != None:
      minimum = self.minimum

    # Fill legend:
    # Data first
    for h in self.hists:
      if isinstance(h, THStack):
        continue
      if not hasattr(h, "drawOption_"):
        h.drawOption_ = ""
      if dataLikeName(h.GetName()):
        self.leg.AddEntry(h, h.GetName(), "pel")

    # Stacked histograms
    for h in self.histsToStack[-1::-1]:
        # h.SetLineColor(0)
        self.leg.AddEntry(h, h.GetName(), "f")

    # Other histograms
    for h in self.hists:
      if not h.GetName():
        continue
      if isinstance(h, THStack):
        continue
      if dataLikeName(h.GetName()):
        continue

      if "p" in h.drawOption_:
        self.leg.AddEntry(h, h.GetName(), "ep")
      elif "e2" in h.drawOption_:
        h.SetLineColor(0)
        # self.leg.AddEntry(h, h.GetName(), "epf")
        self.leg.AddEntry(h, h.GetName(), "ef")
        # self.leg.AddEntry(h, h.GetName(), "pze0")
      else:
        self.leg.AddEntry(h, h.GetName(), "l")
        # self.leg.AddEntry(h, h.GetName(), "ef")

    # change the order for drawing
    self.hists.reverse()
    for ih, h in enumerate(self.hists):
      if dataLikeName(h.GetName()) and integerContent(h, True) and divideByBinWidth:
        gr = TGraphAsymmErrors(h)
        saveStuff.append(gr)
        drawOpt(gr, "Data")
        for p in range(gr.GetN()):
          bw = h.GetBinWidth(p + 1)
          entries = int(round(gr.GetY()[p] * bw))
          edn, eup = getPoissonUnc(entries)
          gr.SetPointEYlow(p, edn / bw)
          gr.SetPointEYhigh(p, eup / bw)
        gr.Draw(gr.drawOption_ + "same")
        # gr.RedrawAxis()
      else:
        if not ih:
          if minimum != None:
            h.SetMinimum(minimum)
          if maximum != None and maximum > minimum:
            h.SetMaximum(maximum)
        else:
          h.drawOption_ += "same"
        h.Draw(h.drawOption_)
        # h.GetXaxis().RedrawAxis()

    # gPad.RedrawAxis()
    gPad.RedrawAxis()

    # self.leg.SetNColumns(2)
    # self.leg.SetNColumns(1)
    self.leg.SetNColumns(legColumns)
    # self.leg.SetNColumns(3)
    # self.leg.Draw()
    self.leg.Draw("same")

    return True

  def draw(self):  # simple alias
    self.Draw()

class Label:
  cmsEnergy = 13  # TeV

  def draw(self):
    varDict = vars(self)
    for varName, obj in varDict.iteritems():
      if isinstance(obj, TLatex):
        obj.SetNDC()
        obj.Draw()

  def __init__(self, drawAll=True, sim=False, status="", info="", year=None):
    if year=="2018":
      intLumi=intLumi18
    elif year=="2017":
      intLumi=intLumi17
    elif year=="2016APV":
      intLumi=intLumi16APV
    elif year=="2016":
      intLumi=intLumi16
    elif year=="FR2":
      intLumi=intLumiFR2
    else:
      intLumi=0.
    saveStuff.append(self)
    # if status == "Private Work":
    if status == "WIP":
      if sim:
        # self.cms = TLatex( 0.2, .887, "#scale[0.76]{#font[52]{Private Work Simulation}}" )
        # self.cms = TLatex( 0.2, .95, "#scale[0.76]{#font[52]{Private Work Simulation}}" )
        # self.cms = TLatex( 0.2, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress Simulation}}" )
        # self.cms = TLatex( 0.15, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress Simulation}}" )
        self.cms = TLatex(
          # 0.16, .92, "#splitline{#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}}{#scale[0.76]{#font[52]{  Simulation}}}")
          # 0.16, .95, "#splitline{#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}}{#scale[0.76]{#font[52]{  Simulation}}}")
          # 0.16, .92, "#splitline{#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}}{#scale[0.76]{#font[52]{  Simulation}}}")
          0.18, .85, "#splitline{#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}}{#scale[0.76]{#font[52]{  Simulation}}}")
      else:
        # self.pub = TLatex( 0.2, .887, "#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}")
        # self.pub = TLatex(
        #     0.15, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{Work in Progress}}")
        self.pub = TLatex(
          # 0.28, .95, "#font[61]{CMS} #scale[0.68]{#font[52]{Work in Progress}}")
          # 0.16, .95, "#font[61]{CMS} #scale[0.68]{#font[52]{Work in Progress}}")
          # 0.18, .85, "#font[61]{CMS} #scale[0.68]{#font[52]{Work in Progress}}")
          0.18, .87, "#font[61]{CMS} #scale[0.68]{#font[52]{Work in Progress}}")
    else:
      if sim:
        self.cms = TLatex(
          # 0.16, .948, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .948, "#scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .95, "#scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.18, .87, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.18, .85, "#splitline{#font[61]{CMS}}{#scale[0.76]{#font[52]{  Simulation}}}")
          0.19, .85, "#splitline{#font[61]{CMS}}{#scale[0.76]{#font[52]{  Simulation}}}")
        # self.sim = TLatex(
        #     0.16, 0.902, "#scale[0.76]{#font[52]{  Simulation}}")
        # 0.2, .887, "#font[61]{CMS} #scale[0.76]{#font[52]{Simulation}}")
      else:
        # self.cms = TLatex(0.2, .887, "#font[61]{CMS}")
        self.cms = TLatex(
          # 0.16, .948, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .948, "#scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .95, "#scale[0.76]{#font[52]{%s}}" % status)
          # 0.16, .95, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          # 0.18, .87, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
          0.19, .87, "#font[61]{CMS} #scale[0.76]{#font[52]{%s}}" % status)
      # self.pub = TLatex(
      #     0.2, .857, "#scale[0.76]{#font[52]{%s}}" % status)
    if year:
      if year=="FR2":
        # self.lum = TLatex(.62, .95,
        #                        "%.1f fb^{-1} (%s TeV)" % (np.round(intLumi / 1000., 0), self.cmsEnergy))
        # self.lum = TLatex(.71, .95,"#scale[0.72]{%.1f fb^{-1} (%s TeV)}" % (np.round(intLumi / 1000., 0), self.cmsEnergy))
        lText = '{:.0f}'.format(intLumi / 1000.)
        self.lum = TLatex(.69, .95,"#scale[0.72]{%.0f fb^{-1} (%s TeV)}" % (intLumi / 1000., self.cmsEnergy))
      else:
        # self.lum = TLatex(.62, .95, "%.1f fb^{-1} (%s TeV)" % (intLumi / 1000., self.cmsEnergy))
        # self.lum = TLatex(.67, .95, "#scale[0.72]{%.1f fb^{-1} (%s TeV)}" % (intLumi / 1000., self.cmsEnergy))
        self.lum = TLatex(.71, .95, "#scale[0.72]{%.1f fb^{-1} (%s TeV)}" % (intLumi / 1000., self.cmsEnergy))
    if info:
      # self.info = TLatex(.15, .95, info)
      self.info = TLatex(.16, .95, info)
    #if info: self.info = TLatex( .85, .85, info )

    if drawAll:
      self.draw()

def save(name, folder="plots/", endings=[".pdf"], normal=False, log=True, changeMinMax=True):
  name = modifySaveName(name)
  if normal:
    for ending in endings:
      gPad.GetCanvas().SaveAs(folder + name + ending)
  if log:
    allH2s = [i for i in gPad.GetCanvas().GetListOfPrimitives() if isinstance(i, TH2)]
    if allH2s:
      gPad.GetCanvas().SetLogz()
    else:
      if changeMinMax:
        setMinMaxForLog()
      gPad.GetCanvas().SetLogy(True)
    for ending in endings:
      gPad.GetCanvas().SaveAs(folder + name + "_log" + ending)
  gPad.GetCanvas().SetLogy(False)

def get_hvalues(infile, histos, hdict, name = "MTop"):
  """
  input: fit diagnostic file from 'combine -M MultiDimFit' with '--saveWorkspace --saveFitResult'
  """
  if not os.path.exists(infile):
    return False

  fitFile = TFile(infile, "READ")
  fitResult = fitFile.Get("fit_mdf")

  if fitResult == None:
    return False

  if fitResult.status() != 0 and fitResult.status() != 1:
    return False

  # if list of params is given, directly search for these params e.g. ['rT', 'a']
  var0 = fitResult.floatParsFinal().find(name)
  if var0 != None:
    hai = var0.getErrorHi()
    lou = var0.getErrorLo()
    # Exclude unconverged fits
    if np.abs(hai) > 0.00000001 and np.abs(lou) > 0.00000001:
      histos[0].Fill(var0.getValV())
      histos[1].Fill(hai)
      histos[2].Fill(lou)

  # if list of params is given, directly search for these params e.g. ['rT', 'a']
  for key in hdict:
    var = fitResult.floatParsFinal().find(key)
    if var != None:
      hs = hdict[key]
      hs[0].Fill(var.getValV())
      hs[1].Fill(var.getErrorHi())
      hs[2].Fill(var.getErrorLo())

  fitFile.Close()
  return True

def get_values(input, symbol="MTop", params=None):
  """
  input: fit diagnostic file from 'combine -M MultiDimFit' with '--saveWorkspace --saveFitResult'
  """
  fitDiagnosticsFile = TFile(input, "READ")
  fitResult = fitDiagnosticsFile.Get("fit_mdf")

  vals = []
  errHi = []
  errLo = []
  err = []

  # if list of params is given, directly search for these params e.g. ['rT', 'a']
  if params:
    for p in params:
      var = fitResult.floatParsFinal().find(p)
      if var != None:
        vals.append(var.getValV())
        errLo.append(var.getErrorLo())
        errHi.append(var.getErrorHi())
        err.append(var.getError())
  else:
    # otherwise look for symbol correlation matrix
    i = 0
    while True:
      var = fitResult.floatParsFinal().find(symbol+str(i))
      if var == None:
        break;

      vals.append(var.getValV())
      errLo.append(var.getErrorLo())
      errHi.append(var.getErrorHi())
      err.append(var.getError())

      i+=1

  fitDiagnosticsFile.Close()
  return np.array(vals), np.array(errLo), np.array(errHi), np.array(err)

def get_correlation_matrix(input, numberOfGenBins=4, symbol="r", params=None):
  """
  input: multidim fit file from 'combine -M MultiDimFit' with '--saveWorkspace --saveFitResult'
  """
  fitDiagnosticsFile = TFile(input, "READ")
  fitResult = fitDiagnosticsFile.Get("fit_mdf")

  if params:
    corrMat = np.empty((len(params),len(params)))
    for i, p1 in enumerate(params):
      for j, p2 in enumerate(params):
        corrMat[i][j] = fitResult.correlation(p1,p2)
  else:
    corrMat = np.empty((numberOfGenBins,numberOfGenBins))

    # correlation matrix
    for i in range(numberOfGenBins):
      for j in range(numberOfGenBins):
        corrMat[i][j] = fitResult.correlation(symbol+str(i),symbol+str(j))

  return corrMat

def get_covariance_matrix(corrMat, errors, numberOfGenBins=4):
  # covariance matrix
  covMat = np.empty((numberOfGenBins,numberOfGenBins))
  for i in range(numberOfGenBins):
      for j in range(numberOfGenBins):
          covMat[i][j] = corrMat[i][j]
          covMat[i][j] *= errors[i]
          covMat[i][j] *= errors[j]

  return covMat

def main():
  ### args
  parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument('-o', '--output', dest='output', action='store', default="ToyPlots",
                      help='path to output directory')
  parser.add_argument('-i', '--input', dest='input', action='store', required=True,
                      help='shape of where to generate toys from')
  if len(sys.argv)==1:
    print ("ERROR: Not enough arguments provided!")
    parser.print_help()
    parser.exit()
  opts, opts_unknown = parser.parse_known_args()

  outFolder = opts.output + "/Toys/"
  if not os.path.exists(outFolder):
    os.makedirs(outFolder)

  POIs = ["MTop"]
  nPOIs = len(POIs)

  h_mass = TH1F("MTop", "", 400, -0.1, 0.1)
  h_mass_errUp = TH1F("MTop_errUp", "", 300, 0, 0.3)
  h_mass_errDn = TH1F("MTop_errDn", "", 300, -0.3, 0.)
  histos = [h_mass, h_mass_errUp, h_mass_errDn]

  histosNuis = {}

  for nuis in nuisances:
    histosNuis[nuis] = []
    histosNuis[nuis].append(TH1F(nuis, "", 400, -0.2, 0.2))
    histosNuis[nuis][-1].SetDirectory(0)
    histosNuis[nuis].append(TH1F(nuis+"_up", "", 210, 0., 1.05))
    histosNuis[nuis][-1].SetDirectory(0)
    histosNuis[nuis].append(TH1F(nuis+"_low", "", 210, -1.05, 0.))
    histosNuis[nuis][-1].SetDirectory(0)

  #collect inputs
  fileList = glob.glob(opts.input + "/multidimfitstats*")
  counter = 0
  for filemon in fileList:
    if get_hvalues(filemon, histos, histosNuis):
      counter += 1
      #corrMat = get_correlation_matrix(file, nPOIs, "MTop")
      #covMat = get_covariance_matrix(corrMat, err_total, len(err_total))
      if counter == 4800:
        break

  can = TCanvas()
  for direction in ["bestfit", "low", "up"]:
    can.Clear()
    if direction == "bestfit":
      h = histos[0]
    elif direction == "up":
      h = histos[1]
    elif direction == "low":
      h = histos[2]
    m = Multiplot()
    h.GetYaxis().SetTitle("Toy Experiments")
    h.GetXaxis().SetTitle("Fitted "+h.GetName())
    drawOpt(h,"signal")
    h.SetLineColor(kBlue+1)
    m.add(h, "Toy Fits to Asimov")

    mean = h.GetMean()
    rms = h.GetRMS()
    std = h.GetStdDev()
    nEntries = h.GetEntries()

    entriesLatex = TLatex()
    entriesLatex.SetTextSize(0.65 * entriesLatex.GetTextSize())
    meanLatex = TLatex()
    meanLatex.SetTextSize(0.65 * meanLatex.GetTextSize())
    rmsLatex = TLatex()
    rmsLatex.SetTextSize(0.65 * rmsLatex.GetTextSize())
    stdLatex = TLatex()
    stdLatex.SetTextSize(0.65 * stdLatex.GetTextSize())

    if m.Draw():
      info = ""
      entriesLatex.DrawLatexNDC(0.23, 0.75, "entries: "+str(int(nEntries)))
      meanLatex.DrawLatexNDC(0.23, 0.70, "mean: "+str(np.round(mean,6)))
      # stdLatex.DrawLatexNDC(0.23, 0.65, "stdDev: "+str(np.round(std,6)))
      rmsLatex.DrawLatexNDC(0.23, 0.65, "rms: "+str(np.round(rms,6)))
      l = Label(status="WIP",info="#scale[0.7]{%s}" % info, sim=False, year="")
      saveName = "MTop_"+direction
      save(saveName, folder=outFolder, normal=True, log=False)

  for iNuis in range(len(nuisances)):
    nuis = nuisances[iNuis]
    for idir,direction in enumerate(["bestfit","up","low"]):
      can.Clear()
      m = Multiplot()
      histosNuis[nuis][idir].GetYaxis().SetTitle("Toy Experiments")
      histosNuis[nuis][idir].GetXaxis().SetTitle("Fitted "+histosNuis[nuis][idir].GetName())
      histosNuis[nuis][idir]
      drawOpt(histosNuis[nuis][idir],"signal")
      histosNuis[nuis][idir].SetLineColor(kBlue+1)
      m.add(histosNuis[nuis][idir], "Toy Fits to Asimov")

      mean = histosNuis[nuis][idir].GetMean()
      rms = histosNuis[nuis][idir].GetRMS()
      std = histosNuis[nuis][idir].GetStdDev()
      nEntries = histosNuis[nuis][idir].GetEntries()

      entriesLatex = TLatex()
      entriesLatex.SetTextSize(0.65 * entriesLatex.GetTextSize())
      meanLatex = TLatex()
      meanLatex.SetTextSize(0.65 * meanLatex.GetTextSize())
      rmsLatex = TLatex()
      rmsLatex.SetTextSize(0.65 * rmsLatex.GetTextSize())
      stdLatex = TLatex()
      stdLatex.SetTextSize(0.65 * stdLatex.GetTextSize())

      if m.Draw():
        info = ""
        entriesLatex.DrawLatexNDC(0.23, 0.75, "entries: "+str(int(nEntries)))
        meanLatex.DrawLatexNDC(0.23, 0.70, "mean: "+str(np.round(mean,6)))
        # stdLatex.DrawLatexNDC(0.23, 0.65, "stdDev: "+str(np.round(std,6)))
        rmsLatex.DrawLatexNDC(0.23, 0.65, "rms: "+str(np.round(rms,6)))
        l = Label(status="WIP",info="#scale[0.7]{%s}" % info, sim=False, year="")
        saveName = nuis + "_" + direction
        save(saveName, folder=outFolder, normal=True, log=False)

  # can.Clear()
  # s = style.style2d()
  # h2_r0_r1.GetXaxis().SetTitle("Fitted r0")
  # h2_r0_r1.GetYaxis().SetTitle("Fitted r1")
  # gStyle.SetPalette(kThermometer)
  # h2_r0_r1.Draw("col")
  # info = ""
  # nEntries = h2_r0_r1.GetEntries()
  # entriesLatex = TLatex()
  # entriesLatex.SetTextSize(0.65 * entriesLatex.GetTextSize())
  # entriesLatex.DrawLatexNDC(0.23, 0.75, "entries: "+str(int(nEntries)))
  # corrLatex = TLatex()
  # corrLatex.SetTextSize(0.65 * corrLatex.GetTextSize())
  # corrLatex.DrawLatexNDC(0.23, 0.70, "correlation: "+str(np.round(h2_r0_r1.GetCorrelationFactor(),6)))
  # l = Label(info="#scale[0.7]{%s}" % info, sim=False, year="")
  # saveName = "ToyStudy_r0_r1"
  # save(saveName, folder=outFolder+"/correlations/", normal=True, log=False)

if __name__ == "__main__":
  main()
