#!/bin/bash
# 0=speed, 1=balance, 2=robustness
STRAT=0
# Default: 0.1
TOL=0.01
# Default batch of settings:
Settings1="-m 125 --cminPreScan --cminFallbackAlgo Minuit2,Combined,2:0.3 --cminDefaultMinimizerTolerance $TOL --cminDefaultMinimizerStrategy $STRAT --X-rtd MINIMIZER_MaxCalls=999999999 --cminDefaultMinimizerPrecision 1E-12"
Settings2="--redefineSignalPOIs MTop --freezeParameters r --setParameters MTop=0,r=1 --setParameterRanges MTop=-1,1 --saveFitResult --robustFit 1 --stepSize=0.005 -v 0"

wait
if [[ "$Mode" == "exp" ]]; then
    Sets="$Settings2 -t -1 $Settings1"
else
    Sets="$Settings2 $Settings1"
fi
wait
combine -M MultiDimFit --algo singles -d space_${1}.root $Sets -n stats
wait
