#!/usr/bin/env python
from ROOT import *
#PyConfig.IgnoreCommandLineOptions = False
gROOT.gPrintViaErrorHandler = True
gROOT.gPrintViaErrorHandler = True
gROOT.SetBatch(True)
gROOT.SetBatch(True)
from array import array
import numpy as np
import sys
import os, subprocess
import argparse
import gc
gc.enable()

def createSingleToy(inPath, outPath, folders, years, channels, samps, seedNumber, syncCat3 = False, onlyMTop = False):
  print("Creating Toy Shape file for seed {}".format(seedNumber))
  # Set random seed
  rando = TRandom()
  rando.SetSeed(int(seedNumber))

  # So i started looping
  for dirLoc in folders:
    inLoc = inPath + dirLoc + "/"
    outLoc = outPath + dirLoc + "/"
    if not os.path.exists(inLoc):
      print("Input path {} does not exist, exiting!".format(inLoc))
      break
    if not os.path.exists(outLoc):
      os.makedirs(outLoc)

    for year in years:
      for chan in channels:
        fName = chan + "_" + year + ".root"

        # Create outfile and load input shapes
        inFile = TFile(inLoc + fName, "READ")
        outFile = TFile(outLoc + fName,"RECREATE")
        outFile.cd()

        # Initializing Asimov
        hInAsimov = inFile.Get("data_obs")
        hOutAsimov = hInAsimov.Clone()
        hOutAsimov.Reset("ICESM") # clear the contents of all histograms
        numBins = hInAsimov.GetNbinsX()

        # Loop over all signal and background processes
        for proc in samps:
          # The original histogram!
          hInNom = inFile.Get(proc)
          hOutNom = hInNom.Clone()
          hOutNom.Reset("ICESM")

          scales = []
          for binX in range(1, numBins + 1):
            oldEntries = hInNom.GetBinContent(binX)
            if oldEntries == 0.:
              scales.append(1.)
              continue
            oldErr = hInNom.GetBinError(binX)

            # We need to consider the effective number of entries to get the correct Poisson distribution.
            effEntries = oldEntries * oldEntries / oldErr
            # The scaling effect is the same for effective and true entry numbers.
            scale = rando.PoissonD(effEntries) / effEntries
            scales.append(scale)
            hOutNom.SetBinContent(binX, oldEntries * scale)
            hOutNom.SetBinError(binX, oldErr * scale**0.5)

          # Collect Asimov
          hOutAsimov.Add(hInNom)

          # Write results
          hOutNom.Write(proc)

          # Run systematics
          systDict = samps[proc]
          for syst in systDict:
            currSyst = systDict[syst]
            # Check if this is an electron-only or muon-only systematic: skip if necessary.
            if chan == "muo":
              if currSyst["eleonly"]:
                continue
            elif chan == "ele":
              if currSyst["muoonly"]:
                continue
            else:
              print("Unknown lepton type!",chan,"Skipping!")
              continue

            for direction in ["Up","Down"]:
              hName = proc + "_" + syst + ("" if currSyst["corr"] else "_" + year) + direction
              hIn = inFile.Get(hName)
              hOut = hIn.Clone()
              hOut.Reset("ICESM")

              # Non-cat3 systematics and Down-variations of cat3 systematics are always synchronized!
              synchronize = (not currSyst["cat3"]) or (direction == "Down" and not currSyst["symm"])
              # Special cases in which we synchronize also cat3
              if not synchronize:
                if syncCat3 or (onlyMTop and syst != "MTop"):
                  synchronize = True

              for binX in range(1, numBins + 1):
                oldEntries = hIn.GetBinContent(binX)
                if oldEntries == 0.:
                  continue
                oldErr = hIn.GetBinError(binX)

                if synchronize:
                  # Synchronize with the central sample variations
                  scale = scales[binX - 1]
                else:
                  # We need to consider the effective number of entries to get the correct Poisson distribution.
                  effEntries = oldEntries * oldEntries / oldErr
                  # The scaling effect is the same for effective and true entry numbers.
                  scale = rando.PoissonD(effEntries) / effEntries
                hOut.SetBinContent(binX, oldEntries * scale)
                hOut.SetBinError(binX, oldErr * (scale**0.5))

              # Write results
              hOut.Write(hName)

        # Write Asimov
        for binX in range(1, numBins + 1):
          hOutAsimov.SetBinError(binX, hInAsimov.GetBinError(binX))
        hOutAsimov.Write("data_obs")

        # Close
        outFile.Close()
        inFile.Close()

def interlinked(target, procs, syst, onlyEle = False, onlyMuo = False, isCorr = True, isSymm = True, isCat3 = False):
  for proc in procs:
    target[proc][syst] = {
      "corr": isCorr,
      "symm": isSymm,
      "cat3": isCat3,
      "eleonly": onlyEle,
      "muoonly": onlyMuo
    }

def main():
  ### args
  parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument('-o', '--output', dest='output', action='store', default="Toys", help='path to output directory')
  parser.add_argument('-i', '--input', dest='input', action='store', default="../", help='path to input directory')
  parser.add_argument('-f', '--folder', dest='folders',  default=[], required=True, nargs='+', help='space separated list of edited folders')
  parser.add_argument('-y', '--year', dest='years', required=True, nargs='+', default=[], help='space separated list of years')
  parser.add_argument('-c', '--channel', dest='channels',  default=[], required=True, nargs='+', help='space separated list of lepton channels')
  parser.add_argument('-t', '--toy', dest='toy', required=True, action='store', help='index of toy sample to generate')
  parser.add_argument('--syncAll', dest='sync', action='store_true', default=False, help='synchronize all systematics with nominal')
  parser.add_argument('--onlyMTop', dest='onlymt', action='store_true', default=False, help='desynchronize only mt-variations')

  if len(sys.argv)==1:
    print ("ERROR: Not enough arguments provided!")
    parser.print_help()
    parser.exit()

  opts, opts_unknown = parser.parse_known_args()

  # Creating a data-structure for all the samples
  samples = {}
  samples["tt-semil"] = {}
  samples["tt-dilept"] = {}
  samples["st-tW"] = {}
  samples["st-tchan"] = {}
  samples["qcd"] = {}
  samples["w+jets"] = {}
  samples["dy+jets"] = {}
  samples["qcd"] = {}
  samples["tt+V"] = {}
  samples["st-schan"] = {}
  samples["tt-allhad"] = {}
  samples["qcd"] = {}
  samples["ww"] = {}
  samples["wz"] = {}
  samples["zz"] = {}

  # Adding all systematics
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW", "st-tchan"], "MTop", isCat3 = True)
  # FSR
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FSRx2xg")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FSRq2qg")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FSRg2gg")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FSRg2qq")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FSRCNSx2xg")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FSRCNSq2qg")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FSRCNSg2gg")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FSRCNSg2qq")
  # ISR
  interlinked(samples, ["tt-semil", "tt-dilept"], "ISR")
  interlinked(samples, ["st-tW"], "ISRst")
  # Misc
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "L1Prefire")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FactScale")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "RenScale")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "Pileup")
  interlinked(samples, ["tt-semil", "tt-dilept"], "TopPt", isSymm = False)
  # PDF
  interlinked(samples, ["tt-semil", "tt-dilept"], "AlphaS")
  interlinked(samples, ["tt-semil", "tt-dilept"], "PDF")
  interlinked(samples, ["tt-semil", "tt-dilept"], "CT14", isSymm = False)
  interlinked(samples, ["tt-semil", "tt-dilept"], "MMHT2014", isSymm = False)
  # B-fragmentation and tagging
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "BFragPeterson", isSymm = False)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "BFragBL")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "BFragSL")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "BCTagSF_corr")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "BCTagSF", isCorr = False)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "BMisTagSF_corr")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "BMisTagSF", isCorr = False)
  # Flavor JEC
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FlavorPureBottom")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FlavorPureCharm")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FlavorPureQuark")
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "FlavorPureGluon")
  # 100% correlated JEC
  interlinked(samples, ["tt-semil"], "JECAbsoluteMPFBias")
  interlinked(samples, ["tt-semil"], "JECAbsoluteScale")
  interlinked(samples, ["tt-semil"], "JECFragmentation")
  interlinked(samples, ["tt-semil"], "JECSinglePionECAL")
  interlinked(samples, ["tt-semil"], "JECSinglePionHCAL")
  # 50% correlated JEC
  interlinked(samples, ["tt-semil"], "JECPileUpDataMC")
  interlinked(samples, ["tt-semil"], "JECPileUpPtBB")
  interlinked(samples, ["tt-semil"], "JECPileUpPtEC1")
  interlinked(samples, ["tt-semil"], "JECPileUpPtEC2")
  interlinked(samples, ["tt-semil"], "JECPileUpPtRef")
  interlinked(samples, ["tt-semil"], "JECPileUptPtHF")
  interlinked(samples, ["tt-semil"], "JECRelativeFSR")
  interlinked(samples, ["tt-semil"], "JECRelativeJERHF")
  interlinked(samples, ["tt-semil"], "JECRelativePtBB")
  interlinked(samples, ["tt-semil"], "JECRelativePtHF")
  interlinked(samples, ["tt-semil"], "JECRelativeBal")
  # 0% correlated JEC
  interlinked(samples, ["tt-semil"], "JECAbsoluteStat", isCorr = False)
  interlinked(samples, ["tt-semil"], "JECRelativeJEREC1", isCorr = False)
  interlinked(samples, ["tt-semil"], "JECRelativeJEREC2", isCorr = False)
  interlinked(samples, ["tt-semil"], "JECRelativePtEC1", isCorr = False)
  interlinked(samples, ["tt-semil"], "JECRelativePtEC2", isCorr = False)
  interlinked(samples, ["tt-semil"], "JECRelativeSample", isCorr = False)
  interlinked(samples, ["tt-semil"], "JECRelativeStatEC", isCorr = False)
  interlinked(samples, ["tt-semil"], "JECRelativeStatFSR", isCorr = False)
  interlinked(samples, ["tt-semil"], "JECRelativeStatHF", isCorr = False)
  interlinked(samples, ["tt-semil"], "JECTimePtEta", isCorr = False)
  # JER
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "JERInner", isCorr = False)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "JERMid", isCorr = False)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "JEROuter", isCorr = False)
  # MET
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "MET", isCorr = False)
  # Muon-only
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "MuoIDSF", onlyMuo = True)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "MuoIsoSF", onlyMuo = True)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "MuoIDSFStat", onlyMuo = True, isCorr = False)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "MuoIsoSFStat", onlyMuo = True, isCorr = False)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "MuoTrgSF", onlyMuo = True, isCorr = False)
  interlinked(samples, ["tt-semil"], "MuoScaleStat", onlyMuo = True, isCorr = False)
  interlinked(samples, ["tt-semil"], "MuoScaleEWK", onlyMuo = True, isSymm = False)
  interlinked(samples, ["tt-semil"], "MuoScaleEWK2", onlyMuo = True, isSymm = False)
  interlinked(samples, ["tt-semil"], "MuoScaleZPt", onlyMuo = True, isSymm = False)
  interlinked(samples, ["tt-semil"], "MuoScaleDM", onlyMuo = True, isSymm = False)
  # Electron-only
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "EleIDSF", onlyEle = True)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "EleRecoSF", onlyEle = True)
  interlinked(samples, ["tt-semil", "tt-dilept", "st-tW"], "EleTrgSF", onlyEle = True, isCorr = False)
  interlinked(samples, ["tt-semil"], "EleScale", onlyEle = True, isCorr = False)
  interlinked(samples, ["tt-semil"], "EleReso", onlyEle = True)
  # Two-sided troublemakers
  interlinked(samples, ["tt-semil"], "HDAMP", isCat3 = True)
  interlinked(samples, ["tt-semil"], "UETune", isCat3 = True)
  # One-sided troublemakers
  interlinked(samples, ["tt-semil"], "CRQCDBased", isSymm = False, isCat3 = True)
  interlinked(samples, ["tt-semil"], "CRGluonMove", isSymm = False, isCat3 = True)
  interlinked(samples, ["tt-semil"], "ERD", isSymm = False, isCat3 = True)

  i_toy = opts.toy
  outFolder = opts.output+"/"+str(i_toy)+"/"

  if not os.path.exists(outFolder):
    os.makedirs(outFolder)

  subprocess.call("ulimit -s unlimited", shell=True)
  createSingleToy(opts.input, outFolder, opts.folders, opts.years, opts.channels, samples, seedNumber = int(i_toy), syncCat3 = opts.sync, onlyMTop = opts.onlymt)
  # Creating workspace
  currentPath = os.path.dirname(os.path.abspath(__file__)) #+"/../"
  workFolder = outFolder + "work/"
  print(workFolder)
  if not os.path.exists(workFolder):
    os.makedirs(workFolder)
  subprocess.call("rsync -avzh card.txt {}.".format(workFolder), shell=True)
  os.chdir(workFolder)
  wsFile = "space.root"
  if os.path.isfile(wsFile):
    subprocess.call("rm " + wsFile, shell=True)
  subprocess.call("text2workspace.py card.txt -o {} -v 0 -m 125".format(wsFile), shell=True)
  os.chdir(currentPath)

if __name__ == "__main__":
    main()
