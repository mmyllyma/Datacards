# Repository for TOP PAG AN-20-147 datacards

Measurement of the top-quark mass in ttbar events with lepton+jets final states in pp collisions at $\\sqrts$=13 TeV using Ultra-Legacy 2017-2018 data.

## Short introduction

Analysis is split into sub-directories depending on the amount of variables considered. A central variable on the split is fit probability (FP),
which is the probability indicating how little the event kinematics needed to change to match a ttbar-semileptonic event hypothesis. Roughly speaking,
FP=1. means that no changes were made, FP=exp(-0.5)~=0.6 means that kinematics were changed by one sigma etc. (this is a simplified explanation).

Base-block in one dimension:

- 1D: the distribution of mt(fit) values [FP>0.2]

- half: mlb [0.2>FP>0.001] 

- full: mlb [0.2>FP>10^(-30)] 

- mw: only the distribution of hadronic W mass [FP>0.2] reconstructed from the two W jets within the event hypothesis 

- mlbr: only the distribution of mlbr=mlb/mtfit [FP>0.2], where mlb is reconstructed from the lepton and leptonic branch B jet 

- rbq: only the distribution of Rbq=(ptbh+ptbl)/(ptq1+ptq2) [FP>0.2] instead of mlbr, where q1 and q2 refer to the hadronic W jets and bh and bl to the B jets 

Here, the terminology "half" vs. "full" originates from the idea whether we take "half" or "full" mlb statistics using the FP cut.
This nomenclature is now somewhat outdated, as the statistics in "full" are around 4x more than those in "half".

Combined blocks:

- 2D: 1D + mw 

- 3D: 1D + mw + mlbr

- 3Db: 1D + mw + rbq (disfavored w.r.t. 3D)

- 4D: 1D + mw + mlbr + rbq

- 5D: 1D + mw + mlbr + rbq + half

- 5Db: 1D + mw + mlbr + rbq + full

- 4Dhalf: same as 5D, but "half" is in a separate datacard and histogram.

- 4Dfull: same as 5Db, but "full" is in a separate datacard and histogram.

It seems that 4Dhalf/4Dfull provide slightly more stable results in comparison to 5D and 5Db, making it a more motivated choice.
However, 5D and 5Db histograms are more convenient for visualization, as all variables are found in the same histogram.

On blinding: the histogram data_obs contains a sum of the MC histograms, with the integral and errors scaled to match data.
Moreover, the histogram data_mctruth can be included, where the original (unscaled) mc histograms are provided.
When unblinding becomes relevant, the histogram data_truth is included.
To use it, the datacards must be appended with something of the sorts:

shapes data_obs lep_2018 lep_2018.root data_true

To be completely clear, the final analysis is to be made in one of the modes 5D/5Db/4Dhalf/4Dfull.
All the other analysis folders and datacards only provide a "piece" of the full truth.
These are necessary for gaining understanding of all the analysis "dimensions".

## Combine instructions

**1. Fetch the repository on gitlab: https://gitlab.cern.ch/hsiikone/Datacards/-/tree/AN-20-147**

```bash
git clone -b AN-20-147 ssh://git@gitlab.cern.ch:7999/hsiikone/Datacards.git #SSH
#OR:
git clone https://gitlab.cern.ch/hsiikone/Datacards.git #HTTPS
git checkout -b AN-20-147
```

Combine should be installed as instructed in https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/:

```bash
git clone https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git HiggsAnalysis/CombinedLimit
cd HiggsAnalysis/CombinedLimit
git fetch origin
git checkout v8.2.0
cd ../..
scram b clean
scram b -j
```

**2. Inspect, create, tune and validate datacards (optional)**

- We have four main bins (channels): muon and electron for both UL17 and UL18

- In addition, mlb can be treated in a separate bin (having a separate phase-space region through the FP cut), adding four extra bins

- For convenience, Datacards are created using CombineHarvester (see DatacardCreation/bin)

- Datacards are provided for all bins separately and for their combination

- The folder scripts contains various scripts for streamlining the Datacard production process 

- half/full/mw/mlbr/rbq/1D/2D/3D/3Db/4D/4Dhalf/4Dfull/5D/5Db

Edit DatacardCreation/bin/TopMass.cpp as required. Install CombineHarvester as instructed in https://cms-analysis.github.io/CombineHarvester/ and build everything:

```bash
git clone https://github.com/cms-analysis/CombineHarvester.git CombineHarvester
scram b -j
```

Remove the old datacards 

```bash
bash scripts/filetools/clean.sh
```

Edit the datacard creation script if necessary, and create new datacards (includes validation, edits and AutoMCStats):

```bash
bash scripts/filetools/datacards.sh
```

or in a streamlined step doing "everything", after the location for the analysis histograms is specified:

```bash
bash scripts/fetcher.sh [NBins]
```

If validation is to be re-done afterwards, use:

```bash
bash scripts/filetools/validation.sh 
```

**3. Create workspaces**

```bash
bash scripts/workspaces.sh 
```

**4. Calculate Impacts with Asimov Data**

```bash
bash scripts/impacts_asimov.sh 
```

**5. Run FitDiagnostics**

This includes e.g. correlations between nuisance parameters.

```bash
bash scripts/diagnostics_asimov.sh 
```

**6. Measurement with Asimov Data**

```bash
bash scripts/measurement_asimov.sh 
```

## Impact plots for publications

- The script [customImpacts.py](https://gitlab.cern.ch/CMS-TOPPAG/Datacards/-/blob/master/Impacts/customImpacts.py) may be used as starting point to generate a nice-looking impact plot to be included in a CMS publication.

- This script requires both [combine](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit) and [combineHarvester](http://cms-analysis.github.io/CombineHarvester) to be installed (see documentations).

- The script requires both the observed and expected json output files obtained from the [Impacts](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part3/nonstandard/#nuisance-parameter-impacts) method in combine.

Run the script with the relevant arguments. Adapt the input/output files, the translation dictionnary, etc.
```
#-- Example 1 (running on dummy inputs, with POI=cpq3)
python Impacts/customImpacts.py --input Impacts/impacts_cpq3Obs.json --asimov-input Impacts/impacts_cpq3Exp.json -o test --onlyfirstpage --POI cpq3 --translate Impacts/rename.json

#-- Example 2 (not including best-fit value and Internal label)
python Impacts/customImpacts.py --input Impacts/impacts_cpq3Obs.json --asimov-input Impacts/impacts_cpq3Exp.json -o test --onlyfirstpage --POI cpq3 --translate Impacts/rename.json --blind --cms-label ''
```

Running these two commands generates these plots: [1](https://gitlab.cern.ch/CMS-TOPPAG/Datacards/-/blob/master/Impacts/example_internal.pdf), [2](https://gitlab.cern.ch/CMS-TOPPAG/Datacards/-/blob/master/Impacts/example_blind_pub.pdf).
