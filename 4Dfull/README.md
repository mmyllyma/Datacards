# 5D fits: mtfit, mwreco, rbreco, mlbreducedreco and mlbreco

For mlbreco, 0.2>fitProb>0.001. For all others, fitProb>0.2.
Here, the modes '4D' and 'full' are combined manually from two separate histograms,
vs. the one histogram approach found in 5Db.
