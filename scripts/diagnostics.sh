#!/bin/bash
# 0=speed, 1=balance, 2=robustness
STRAT=2
# Default: 0.1
TOL=0.1
# Default batch of settings:
#Settings1="-m 125 --cminPreScan --cminFallbackAlgo Minuit2,Combined,2:0.3 --cminDefaultMinimizerTolerance $TOL --cminDefaultMinimizerStrategy $STRAT --X-rtd MINIMIZER_MaxCalls=999999999 --cminDefaultMinimizerPrecision 1E-12"
#Settings2="--redefineSignalPOIs MTop --freezeParameters r --setParameters MTop=0,r=1 --setParameterRanges MTop=-1,1 --robustFit 1 --stepSize=0.005 -v 2"

Settings1="-m 125 --cminPreScan --cminFallbackAlgo Minuit2,Combined,2:0.3 --cminDefaultMinimizerTolerance $TOL --cminDefaultMinimizerStrategy $STRAT --X-rtd MINIMIZER_MaxCalls=999999999 --cminDefaultMinimizerPrecision 1E-12"
Settings2="--redefineSignalPOIs MTop --freezeParameters r --setParameters MTop=0,r=1 --setParameterRanges MTop=-1,1 --robustFit 1 --stepSize=0.005 -v 2 --robustHesse 1"


# "--minos all" minos for all parameters

# --robustFit -> robust likelihood scan, further controlled with the parameter stepsize=0.1 by default
#   these seem not to do anything: --setRobustFitAlgo, --setRobustFitStrategy, --setRobustFitTolerance

# if fitDiagnostics_exp.root -> fit_s->Print() forced positive-definite 
#   --robustHesse 1
#   covariance matrix quality: Unknown, matrix was externally provided

# --cminDefaultMinimizerStrategy 0 can also help with this problem


Folders="5D"
#Folders="2D 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"
#Folders="2D 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq"
#Folders="4Dhalf"
#Folders="full half 1D 2D 2Dfull 2Dhalf 3D 3Db 4D 4Dfull 4Dhalf 5D 5Db"
Leps="ele"
#Leps="muo"
Years="18"
#Years="18"
# Years="17 18"
#Mode="obs"
Mode="exp"
#Shapes="--saveShapes --saveOverallShapes --saveWithUncertainties --saveNormalizations --numToysForShapes 1000"
Shapes=""

if [[ "$Mode" == "exp" ]]; then
    Sets="$Settings2 -t -1 $Settings1 $Shapes"
else
    Sets="$Settings2 $Settings1 $Shapes"
fi

diagnose() {
    echo "> ${1}"
    Loc="diagnostics_${TOL}/${1}"
    Begin=$PWD
    mkdir -p ${Loc}
    cd ${Loc}
    wait
    #nohup combineTool.py -M FitDiagnostics $Sets --plots --cminPreFit 1 -n _${Mode} -d ../../ws_${1}.root
    combineTool.py -M FitDiagnostics $Sets --plots --cminPreFit 1 -n _${Mode} -d ../../ws_${1}.root
    wait
    python $CMSSW_BASE/src/HiggsAnalysis/CombinedLimit/test/diffNuisances.py -a fitDiagnostics_${Mode}.root -g plots_${Mode}.root -p MTop > log_${Mode}.txt
    wait
    cd $Begin
}

for fol in $Folders; do
    echo $fol
    cd $fol
    for year in $Years; do
        for lep in $Leps; do
            card=${lep}_20${year}
            diagnose $card
            wait
            cd diagnostics_${TOL}/$card
            root -l -b -q ../../../scripts/correlate.C\(\"${Mode}\",\"20${year}\"\)
            cd ../..
            wait
        done
        if [[ ${#Leps} -gt 3 ]]; then
            card=lep_20${year}
            diagnose $card
        fi
    done
    if [[ ${#Years} -gt 2 ]]; then
        for lep in $Leps; do
            diagnose $lep
        done
        diagnose lep
        wait
    fi
    cd ..
done

