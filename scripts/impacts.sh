#!/bin/bash
# 0=speed, 1=balance, 2=robustness
STRAT=1
# Default: 0.1
TOL=0.01
# Default batch of settings:
Settings1="-m 125 --cminPreScan --cminFallbackAlgo Minuit2,Combined,2:0.3 --cminDefaultMinimizerTolerance $TOL --cminDefaultMinimizerStrategy $STRAT --X-rtd MINIMIZER_MaxCalls=999999999 --cminDefaultMinimizerPrecision 1E-12"
Settings2="--redefineSignalPOIs MTop --freezeParameters r --setParameters MTop=0,r=1 --setParameterRanges MTop=-1,1 --saveWorkspace --saveFitResult --robustFit 1 --stepSize=0.005 -v 2"

#Settings2="--redefineSignalPOIs MTop --freezeParameters r,var{prop.*} --setParameters MTop=0,r=1 --setParameterRanges MTop=-1,1 --saveWorkspace --saveFitResult --robustFit 1 --stepSize=0.005 -v 2"
#Settings2="--redefineSignalPOIs MTop --freezeParameters allConstrainedNuisances --setParameters MTop=0,r=1 --setParameterRanges MTop=-1,1 --saveWorkspace --saveFitResult --robustFit 1 --stepSize=0.005 -v 2"
#Settings2="--redefineSignalPOIs MTop --freezeParameters r --setParameters MTop=0,r=1 --setParameterRanges MTop=-3,3 --saveWorkspace --saveFitResult --robustFit 1 --stepSize=0.005 -v 2"
#Settings2="--redefineSignalPOIs MTop --freezeParameters allConstrainedNuisances --setParameters MTop=0,r=1 --setParameterRanges MTop=-3,3 --saveWorkspace --saveFitResult --robustFit 1 --stepSize=0.005 -v 2"


#Folders="2D 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"
#Folders="mtwrbq"
#Folders="kdTree"
#Folders="mtwrbqlb"
Folders="5D"
#Folders="2D 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"
#Folders="2D 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq"

#Folders="half full 1D mw mlbr rbq 4Dhalf 4Dfull"
#Folders="full half 1D 2D 2Dfull 2Dhalf 3D 3Db 4D 4Dfull 4Dhalf 5D 5Db"
#Leps="ele"
#Leps="muo"
Leps="ele muo"
#Years="17"
#Years="16APV 16"
#Years="16"
#Years="16APV"
#Years="18"
Years="16APV 16 17 18"
#Years="17 18"
#Modes="obs"
Modes="exp"
#Modes="exp obs"

impact() {
    echo "> ${1}"
    Loc="impacts/${1}"
    Begin=$PWD
    mkdir -p ${Loc}
    cd ${Loc}
    for mode in $Modes; do
        mkdir -p $mode
        wait
        cd $mode
        if [[ "$mode" == "exp" ]]; then
            Sets="$Settings2 -t -1 $Settings1"
        else
            Sets="$Settings2 $Settings1"
        fi

        echo ${Sets}

        # http://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part3/nonstandard/#nuisance-parameter-impacts

        wait
        # Initial step:
        combineTool.py -M Impacts $Sets -d ../../../ws_${1}.root --doInitialFit > impactInitial_${mode}.log
        wait
        # Loop over nuisances:
        combineTool.py -M Impacts $Sets -d ../../../ws_${1}.root --doFits --parallel 20 --cminPreFit 1
        wait
        # Collect:
        combineTool.py -M Impacts -d ../../../ws_${1}.root -m 125 -o ../impacts_${mode}.json --redefineSignalPOIs MTop --freezeParameters r
        wait
        # Dropping the dummy 'r' variable:
        jq 'del(.params[] | select(.name == "r"))' ../impacts_${mode}.json > ../../impacts_${1}_${mode}.json
        wait
        # Plot
        plotImpacts.py -i ../../impacts_${1}_${mode}.json --POI MTop --units [GeV] -t ../../../../scripts/plotting/rename.json -o ../../${1}_${mode}
        plotImpacts.py -i ../../impacts_${1}_${mode}.json --POI MTop --units [GeV] -t ../../../../scripts/plotting/rename.json -o ../../${1}_${mode}_short --cms-label "Work in Progress" --per-page 20 --height 600 --label-size 0.045 --max-pages 1
        #plotImpacts.py -i ../../impacts_${1}_${mode}.json --POI MTop --units [GeV] -t ../../../../scripts/plotting/rename.json -o ../../${1}_${mode}_verbose --cms-label "    Internal" --per-page 80 --height 2000 --label-size 0.047 --nolegend

        # nolegend flag removed:
        plotImpacts.py -i ../../impacts_${1}_${mode}.json --POI MTop --units [GeV] -t ../../../../scripts/plotting/rename.json -o ../../${1}_${mode}_verbose --cms-label "    Internal" --per-page 80 --height 2000 --label-size 0.047
        wait
        cd ..
        wait
    done
    if [[ ${#Modes} -gt 3 ]]; then
        cd ..
        wait
        #python ../../scripts/plotting/customImpacts.py --input impacts_${1}_obs.json --asimov-input impacts_${1}_exp.json -o cmsimpacts --POI MTop --units [GeV] --translate ../../scripts/plotting/rename.json #--onlyfirstpage
        python ../../scripts/plotting/customImpacts.py --input impacts_${1}_obs.json --asimov-input impacts_${1}_exp.json -o blindimpacts --POI MTop --units [GeV] --translate ../../scripts/plotting/rename.json --blind --cms-label '' #--onlyfirstpage
        wait
    fi
    cd $Begin
}

for fol in $Folders; do
    echo $fol
    cd $fol
    for year in $Years; do
        echo $year
        for lep in $Leps; do
            card=${lep}_20${year}
            impact $card
        done
        if [[ ${#Leps} -gt 3 ]]; then
            card=lep_20${year}
            impact $card
        fi
    done
    #if [[ ${#Years} -gt 2 ]]; then
    if [[ ${#Years} -gt 2 && $Years != "16APV" ]]; then
        for lep in $Leps; do
            impact $lep
        done
        impact lep
    fi
    cd ..
done
