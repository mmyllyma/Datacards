#!/bin/bash
# Quickly append the desired suffix

#Folders="4Dhalf"
#Folders="full half mw mlbr rbq 1D 2D 2Dfull 2Dhalf 3D 3Db 4D 4Dfull 4Dhalf 5D 5Db"
#Folders="full half mw mlbr rbq 1D 2D 3D 3Db 4D 5D 5Db"
#Folders="full 1D 2D 2Dfull 3D 4D 4Dfull 5D 5Db"
#Folders="mtwrbq"
#Folders="kdTree"
#Folders="mtwrbqlb"
#Folders="3D mtwrbq"
Folders="5D"
#Folders="2D 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"
#Folders="2D 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq"
#Folders="5D 2D 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"
#Folders="2D 2Dmtrbq 2Dmtlbr 2Dmwrbq"

#Years="16"
#Years="17"
#Years="16APV 16"
#Years="17 18"
#Years="18"
Years="16APV 16 17 18"
#Leps="muo"
#Leps="ele"
Leps="muo ele"
for fol in $Folders; do
    echo $fol
    cd $fol
    wait
    for year in $Years; do
        echo $year
        for lep in $Leps; do
            text2workspace.py ${lep}_20${year}.txt -o ws_${lep}_20${year}.root -m 125 -v 0 
            wait
        done
        if [[ ${#Leps} -gt 3 ]]; then
            text2workspace.py lep_20${year}.txt -o ws_lep_20${year}.root -m 125 -v 0 
            wait
        fi
    done
    if [[ ${#Years} -gt 2 ]]; then
        for lep in $Leps; do
            text2workspace.py ${lep}.txt -o ws_${lep}.root -m 125 -v 0 
            wait
        done
        text2workspace.py lep.txt -o ws_lep.root -m 125 -v 0  
        wait
    fi
    cd ..
    wait
done
