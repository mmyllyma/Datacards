#!/bin/bash
# 0=speed, 1=balance, 2=robustness
STRAT=1
# Default: 0.1
TOL=0.01
# Algo for scans
Algo=grid
# Number of points
POINTS=30
# Default batch of settings:
Settings1="--algo $Algo --points $POINTS -m 125 --cminPreScan --cminFallbackAlgo Minuit2,Combined,2:0.3 --cminDefaultMinimizerTolerance $TOL --cminDefaultMinimizerStrategy $STRAT --X-rtd MINIMIZER_MaxCalls=999999999 --cminDefaultMinimizerPrecision 1E-12"
Settings2="--redefineSignalPOIs MTop --setParameters MTop=0,r=1 --setParameterRanges MTop=-1,1 --saveWorkspace --saveFitResult --robustFit 1 --stepSize=0.005 -v 3"

Folders="4Dhalf"
#Folders="full half 1D 2D 2Dfull 2Dhalf 3D 3Db 4D 4Dfull 4Dhalf 5D 5Db"
#Leps="ele"
#Leps="muo"
Leps="ele muo"
#Years="17"
#Years="18"
Years="17 18"
#Mode="obs"
Mode="exp"

if [[ "$Mode" == "exp" ]]; then
    Sets="$Settings2 -t -1 $Settings1"
else
    Sets="$Settings2 $Settings1"
fi

measure() {
    echo "> ${1}"
    Loc="uncertaintysplit/${1}"
    Begin=$PWD
    mkdir -p $Loc
    cd $Loc
    # Expected results:
    nohup combine -M MultiDimFit $Sets -n _${Mode} -d ../../ws_${1}.root --freezeParameters r >& fit_${Mode}.txt&
    wait
    # Statistical ONLY results:
    nohup combine -M MultiDimFit $Sets -n _${Mode}_stat -d higgsCombine_${Mode}.MultiDimFit.mH125.root --X-rtd --freezeParameters r,allConstrainedNuisances --snapshotName MultiDimFit >& fit_${Mode}_stat.txt&
    wait
    # Plot
    plot1DScan.py higgsCombine_${Mode}.MultiDimFit.mH125.root --main-label "Total Uncert." --others higgsCombine_${Mode}_stat.MultiDimFit.mH125.root:"Stat only":6 --output breakdown --breakdown "syst,stat" --POI MTop
    wait
    cd $Begin
}

for fol in $Folders; do
    echo $fol
    cd $fol
    for year in $Years; do
        for lep in $Leps; do
            card=${lep}_20${year}
            measure $card
        done
    done
    if [[ ${#Years} -gt 2 ]]; then
        for lep in $Leps; do
            measure $lep
            wait
        done
        measure lep 
        wait
    fi
    cd ..
done
