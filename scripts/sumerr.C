#include <iostream>
#include "TH1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TString.h"
#include "TStyle.h"
#include <fstream>
#include <algorithm>
#include "TLorentzVector.h"
#include "TLegend.h"
#include "TApplication.h"
#include <vector>
#include <string>

using std::vector;
using std::string;
using std::cout;
using std::endl;

// This is a quick script for calculating the Combine autoMCStats neff values binwise.
void sumerr(const string fName) {
  const string loc = fName.substr(0, fName.rfind("."));
  const string year = loc.substr(fName.rfind("_") + 1, 4);
  gStyle->SetOptStat(0);
  TH1::SetDefaultSumw2();

  TFile *f = new TFile(fName.c_str(), "READ");
  const vector<string> samples = {"tt-semil", "tt-dilept", "st-tW", "st-tchan", "st-schan", "qcd", "w+jets", "dy+jets", "ww", "wz", "zz"};

  int divider = 0;
  if (f && !f->IsZombie() && f->Get("data_obs")) { 
    int nBins = static_cast<TH1F*>(f->Get("data_obs"))->GetNbinsX();

    double cumul = 0.;
    double cumule = 0.;
    for (int i = 1; i <= nBins; ++i) {
      for (const string &sam : samples) {
        TH1F *curr = static_cast<TH1F*>(f->Get(sam.c_str()));
        cumul += curr->GetBinContent(i);
        cumule += pow(curr->GetBinError(i),2);
      }
      cout << "Bin: " << i << " " << pow(cumul, 2)/cumule << endl;
    }
  }
}
