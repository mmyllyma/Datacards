#!/bin/bash

Folders="full half mw mlbr rbq 1D 2D 2Dfull 2Dhalf 3D 3Db 4D 4Dfull 4Dhalf 5D 5Db"
for fol in $Folders; do
    git add -f $fol/*txt
done
git add scripts/*sh
git add scripts/filetools/*sh

