#include "TH1D.h"
#include "TFile.h"

#include <string>
#include <vector>
#include <iostream>

// Temporary fix for downtime

using std::cout;
using std::endl;
using std::vector;
using std::string;

void update5(string fname) {
  cout << "Working..." << fname << endl;
  TFile f(fname.c_str(), "UPDATE");
  vector<string> cases = {"MuoScaleEWK", "MuoScaleEWK2", "MuoScaleZPt", "MuoScaleDM"};
  vector<string> adds = {"tt-semil"};
  
  const char* bonus = "";
  if (!f.IsZombie() && f.Get("data_obs")) {
    for (const auto &cCase : cases) {
      for (const auto &add : adds) {
        auto up = dynamic_cast<TH1F*>(f.Get(Form("%s_%s", add.c_str(), cCase.c_str())));
        auto dn = dynamic_cast<TH1F*>(f.Get(Form("%s", add.c_str())));

        TH1F* dnCopy = dynamic_cast<TH1F*>(dn->Clone("dn_copy"));

        for (int bin = 1; bin <= dnCopy->GetNbinsX(); ++bin) {
            double dnContent = dnCopy->GetBinContent(bin);
            double upContent = up->GetBinContent(bin);

            dnCopy->SetBinError(bin, up->GetBinError(bin));

            if (dnContent < upContent) {
                double diff = (upContent - dnContent);
                dnCopy->SetBinContent(bin, dnContent - diff);
            } else if (dnContent > upContent) {
                double diff = (dnContent - upContent);
                dnCopy->SetBinContent(bin, dnContent + diff);
            }
        }
        if (up && dn) {
          up->Write(Form("%s_%s%sUp", add.c_str(), cCase.c_str(), bonus));
          dnCopy->Write(Form("%s_%s%sDown", add.c_str(), cCase.c_str(), bonus));
        } else {
          cout << "Could not locate nuisance " << cCase << endl;
        }
      }
    }
  }
  f.Close();
}