#!/bin/bash
# Quickly append the desired suffix

#Deep=0
Deep=1

Folders="full half mw mlbr rbq 1D 2D 2Dfull 2Dhalf 3D 3Db 4D 4Db 4Dfull 4Dhalf 5D 5Db 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq mtwrbq kdTree mtwrbqlb"
for fol in $Folders; do
    if [[ -d $fol ]]; then
        echo $fol
        if [[ $Deep -eq 1 ]]; then
            rm $fol/*.root
            rm $fol/*.txt
            rm -rf $fol/impacts
            rm -rf $fol/ele_*
            rm -rf $fol/muo_*
        fi
        wait
        rm $fol/*.pdf
        wait
        rm $fol/ws_*.root
        wait
        Res=$fol/results
        if [[ -d $Res ]]; then
            echo "Removing $Res"
            rm -r $Res
            wait
        fi
        Val=$fol/validation
        if [[ -d $Val ]]; then
            echo "Removing $Val"
            rm -r $Val
            wait
        fi
    else
        echo "Folder $fol nonexistent!"
    fi
done
