#include "TH1D.h"
#include "TFile.h"

#include <string>
#include <vector>
#include <iostream>

// Temporary fix for downtime

using std::cout;
using std::endl;
using std::vector;
using std::string;

void update4(string fname) {
  cout << "Working..." << fname << endl;
  TFile f(fname.c_str(), "UPDATE");
  vector<string> cases = {"BFragBL"};
  vector<string> adds = {"tt-semil", "tt-dilept", "st-tW"};
  //vector<string> adds = {"tt-semil", "tt-semil-CP", "tt-semil-WP", "tt-semil-UN", "tt-dilept", "st-tW"};
  const char* bonus = "s";
  if (!f.IsZombie() && f.Get("data_obs")) {
    for (const auto &cCase : cases) {
      for (const auto &add : adds) {
        auto up = dynamic_cast<TH1F*>(f.Get(Form("%s_%s", add.c_str(), cCase.c_str())));
        auto dn = dynamic_cast<TH1F*>(f.Get(Form("%s", add.c_str())));
        if (up && dn) {
          up->Write(Form("%s_%s%sUp", add.c_str(), cCase.c_str(), bonus));
          dn->Write(Form("%s_%s%sDown", add.c_str(), cCase.c_str(), bonus));
        } else {
          cout << "Could not locate nuisance " << cCase << endl;
        }
      }
    }
  }
  f.Close();
}
