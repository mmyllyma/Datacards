#!/bin/bash
# Quickly append the desired suffix

#Folders="full half 1D 2D 2Dfull 2Dhalf 3D 3Db 4D 4Dfull 4Dhalf 5D 5Db"
Folders="full half 1D 2D 3D 3Db 4D 5D 5Db"
#Folders="full 1D 2D 2Dfull 3D 4D 4Dfull 5D 5Db"
#Folders="5D"
#Years="17"
#Years="17 18"
Years="16APV 16"
#Leps="muo"
#Leps="ele"
Leps="muo ele"

vali() {
    python $CMSSW_BASE/src/HiggsAnalysis/CombinedLimit/test/systematicsAnalyzer.py ${1}.txt --all > validation/nuisancereport_${1}.html
    wait
    ValidateDatacards.py ${1}.txt --printLevel 3 --mass 125 > validation/validateDatacards_${1}.txt
    wait
    mv validation.json validation/validation_${1}.json
    wait
}

for fol in $Folders; do
    echo $fol
    cd $fol
    wait
    mkdir -p validation 
    for year in $Years; do
        echo $year
        for lep in $Leps; do
            card="${lep}_20${year}"
            echo "> ${card}"
            vali $card
        done
    done
    if [[ ${#Years} -gt 2 ]]; then
        for lep in $Leps; do
            vali ${lep}
            wait
        done
        vali lep
        wait
    fi
    cd ..
    wait
done
