#!/bin/bash

Folders="1D 2D 3D 3Db 4D 5D 5Db full half"
for fol in $Folders; do
    rsync -avzh ../$1/$fol/*.root $fol/.
done

