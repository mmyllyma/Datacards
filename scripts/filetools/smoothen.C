#include "TH1D.h"
#include "TH1F.h"
#include "TFile.h"
#include "TSpectrum.h"

#include <string>
#include <vector>
#include <iostream>

// Temporary fix for downtime

using std::cout;
using std::endl;
using std::vector;
using std::string;

void smoothFun(TH1F* target, int multiplier, int rounds) {
  int totBins = target->GetNbinsX();
  int sBins = totBins/multiplier;
  for (int iMult = 0; iMult < multiplier; ++iMult) {
    double totWgt = 0.;
    double offset = 0.;
    for (int iBin = 1; iBin <= sBins; ++iBin) {
      double wgt = target->GetBinError(iMult * sBins + iBin);
      if (wgt > 0.) {
        wgt = 1./pow(wgt, 2);
        totWgt += wgt;
        offset += wgt * target->GetBinContent(iMult * sBins + iBin);
      }
    }
    offset /= totWgt;
    if (rounds == 0) {
      Double_t source[sBins];
      for (int iBin = 1; iBin <= sBins; ++iBin) source[iBin - 1] = target->GetBinContent(iMult * sBins + iBin) - offset;
      TSpectrum *s = new TSpectrum();
      s->SmoothMarkov(source, sBins, 3);
      for (int iBin = 1; iBin <= sBins; ++iBin) {
        target->SetBinContent(iMult * sBins + iBin, source[iBin - 1] + offset);
      }
    } else {
      TH1F deposit("gen", "", sBins, 0, sBins);
      for (int iBin = 1; iBin <= sBins; ++iBin) {
        deposit.SetBinContent(iBin, target->GetBinContent(iMult * sBins + iBin) - offset);
        deposit.SetBinError(iBin, target->GetBinError(iMult * sBins + iBin));
      }
      deposit.Smooth(rounds);
      for (int iBin = 1; iBin <= sBins; ++iBin) {
        target->SetBinContent(iMult * sBins + iBin, deposit.GetBinContent(iBin) + offset);
        target->SetBinError(iMult * sBins + iBin, deposit.GetBinError(iBin));
      }
    }
  }
}

void smoothen(string fname, int rounds = 1) {
  cout << "Working..." << fname << endl;
  const string path = fname.substr(0, fname.rfind("/"));
  const string dimension = path.substr(path.rfind("/") + 1, 1);
  int multiplier = 1;
  try {
    multiplier = std::stoi(dimension);
  } catch (std::exception& e) {
    cout << "Path " << dimension << " not conversible to a numeric dimension, multiplier = 1 is used." << endl;
  }
  TFile f(fname.c_str(), "UPDATE");
  //vector<string> cases = {"UETune", "HDAMP", "CRQCDBased", "CRGluonMove", "ERD"};
  vector<string> cases = {"HDAMP"};
  vector<string> adds = {"tt-semil"};
  //vector<string> adds = {"tt-semil", "tt-semil-CP", "tt-semil-WP", "tt-semil-UN"};
  string bonus = "S" + std::to_string(rounds);
  if (!f.IsZombie() && f.Get("data_obs")) {
    for (const auto &cCase : cases) {
      for (const auto &add : adds) {
        auto upOrig = dynamic_cast<TH1F*>(f.Get(Form("%s_%sUp", add.c_str(), cCase.c_str())));
        auto dnOrig = dynamic_cast<TH1F*>(f.Get(Form("%s_%sDown", add.c_str(), cCase.c_str())));
        auto cnt = dynamic_cast<TH1F*>(f.Get(Form("%s", add.c_str())));
        auto up = dynamic_cast<TH1F*>(upOrig->Clone());
        auto dn = dynamic_cast<TH1F*>(dnOrig->Clone());
        up->Add(cnt, -1);
        dn->Add(cnt, -1);
        smoothFun(up, multiplier, rounds);
        smoothFun(dn, multiplier, rounds);
        up->Add(cnt);
        dn->Add(cnt);
        if (up && dn) {
          up->Write(Form("%s_%s%sUp", add.c_str(), cCase.c_str(), bonus.c_str()));
          dn->Write(Form("%s_%s%sDown", add.c_str(), cCase.c_str(), bonus.c_str()));
        } else {
          cout << "Could not locate nuisance " << cCase << endl;
        }
      }
    }
  }
  f.Close();
}
