#!/bin/bash
# Quickly append the desired suffix

#Folders="full half mw mlbr rbq 1D 2D 3D 3Db 4D 5D 5Db 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"
#Folders="full half mw mlbr rbq 1D 2D 3D 3Db 4D 5D 5Db 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"

#Folders="full half mw mlbr rbq 1D 2D 3D 3Db 4D 5D 5Db mtwrbq mtw"
#Folders="mtwrbq"
#Folders="kdTree"
#Folders="mtwrbqlb"
Folders="5D"
#Folders="2D 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"

if [[ $1 -eq 0 ]]; then
  Leps="muo ele"
elif [[ $1 -eq 1 ]]; then
  Leps="ele"
else
  Leps="muo"
fi

if [[ $2 -eq 0 ]]; then
  Years="17 18"
elif [[ $2 -eq 1 ]]; then
  Years="17"
elif [[ $2 -eq 2 ]]; then
  Years="18"
elif [[ $2 -eq 3 ]]; then
  Years="16"
elif [[ $2 -eq 4 ]]; then
  Years="16APV"
elif [[ $2 -eq 5 ]]; then
  Years="16APV 16"
elif [[ $2 -eq 6 ]]; then
  Years="16APV 16 17 18"
else
  Years="17"
fi

for fol in $Folders; do
    echo $fol
    for year in $Years; do
        for lep in $Leps; do
            root -l -b -q scripts/filetools/update1.C\(\"${fol}/${lep}_20${year}.root\"\)
            root -l -b -q scripts/filetools/update2.C\(\"${fol}/${lep}_20${year}.root\"\)
            root -l -b -q scripts/filetools/update3.C\(\"${fol}/${lep}_20${year}.root\"\)
            root -l -b -q scripts/filetools/update4.C\(\"${fol}/${lep}_20${year}.root\"\)
            if [[ "${lep}" == "muo" ]]; then
                root -l -b -q scripts/filetools/update5.C\(\"${fol}/${lep}_20${year}.root\"\)
            fi
            root -l -b -q scripts/filetools/update6.C\(\"${fol}/${lep}_20${year}.root\"\)
            wait
        done
    done
done
