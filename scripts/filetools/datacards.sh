#!/bin/bash

# Create datacards and quickly append the desired suffix and edit file paths.

if [[ $1 -eq 0 ]]; then
  Leps="muo ele"
elif [[ $1 -eq 1 ]]; then
  Leps="ele"
else
  Leps="muo"
fi

if [[ $2 -eq 0 ]]; then
  Years="17 18"
elif [[ $2 -eq 1 ]]; then
  Years="17"
elif [[ $2 -eq 2 ]]; then
  Years="18"
elif [[ $2 -eq 3 ]]; then
  Years="16"
elif [[ $2 -eq 4 ]]; then
  Years="16APV"
elif [[ $2 -eq 5 ]]; then
  Years="16APV 16"
elif [[ $2 -eq 6 ]]; then
  Years="16APV 16 17 18"
else
  Years="17"
fi

DoValidation=0
#DoValidation=1
#ClearValidation=0
ClearValidation=1

#Folders="full half mw mlbr rbq 1D 2D 3D 3Db 4D 5D 5Db 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"
#Folders="full half mw mlbr rbq 1D 2D 3D 3Db 4D 5D 5Db 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"

#Folders="full half mw mlbr rbq 1D 2D 3D 3Db 4D 5D 5Db mtwrbq"
#Folders="2D 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"

#Folders="full 1D 2D 3D 4D 5D 5Db"
#Folders="kdTree"
#Folders="mtwrbqlb"
Folders="5D"
#FoldersComb="full half"
#FoldersComb="full"


Res=results
Val=validation

tune() {
    # Correct text
    echo "* autoMCStats 1000 1 1" >> ${1}.txt
    wait
    # Replace file locations
    for year in $Years; do
        for lep in $Leps; do
            name=${lep}_20${year}
            sed -i "s/shapes \* ${name} results\/out${2}.root ${name}\/\$PROCESS ${name}\/\$PROCESS_\$SYSTEMATIC/shapes \* ${name} ..\/${3}\/${name}.root \$PROCESS \$PROCESS_\$SYSTEMATIC/" ${1}.txt
            wait
        done
    done
    if [[ DoValidation -eq 1 ]]; then
        # Datacard validation begins
        python $CMSSW_BASE/src/HiggsAnalysis/CombinedLimit/test/systematicsAnalyzer.py ${1}.txt --all > ${Val}/nuisancereport_${1}.html
        wait
        ValidateDatacards.py ${1}.txt --printLevel 3 --mass 125 > ${Val}/validateDatacards_${1}.txt
        wait
        mv validation.json ${Val}/validation_${1}.json
        wait
    fi
}

tunecomb() {
    # Correct text
    echo "* autoMCStats 1000 1 1" >> ${1}.txt
    wait
    # Replace file locations (note special locations of the true .root files)
    for year in $Years; do
        for lep in $Leps; do
            name=${lep}_20${year}
            namem=${lep}mlb_20${year}
            sed -i "s/shapes \* ${name} results\/out${3}.root ${name}\/\$PROCESS ${name}\/\$PROCESS_\$SYSTEMATIC/shapes \* ${name} ..\/${4}\/${name}.root \$PROCESS \$PROCESS_\$SYSTEMATIC/" ${1}.txt
            wait
            sed -i "s/shapes \* ${namem} results\/out${3}.root ${namem}\/\$PROCESS ${namem}\/\$PROCESS_\$SYSTEMATIC/shapes \* ${namem} ..\/${2}\/${name}.root \$PROCESS \$PROCESS_\$SYSTEMATIC/" ${1}.txt
            wait
        done
    done
    if [[ DoValidation -eq 1 ]]; then
        # Datacard validation begins
        python $CMSSW_BASE/src/HiggsAnalysis/CombinedLimit/test/systematicsAnalyzer.py ${1}.txt --all > ${Val}/nuisancereport_${1}.html
        wait
        ValidateDatacards.py ${1}.txt --printLevel 3 --mass 125 > ${Val}/validateDatacards_${1}.txt
        wait
        mv validation.json ${Val}/validation_${1}.json
        wait
    fi
}

for fol in $Folders; do
    echo $fol
    TopMass $fol $1 $2
    wait
    cd $fol
    wait
    if [[ -d $Res ]]; then
        echo "Removing $Res"
        rm ${Res}/*root
        wait
        rmdir $Res
        wait
    fi
    if [[ DoValidation -eq 1 ]]; then
        mkdir -p $Val
    fi
    for yr in $Years; do
        for lept in $Leps; do
            card=${lept}_20${yr}
            echo "> ${card}"
            tune ${card} _${card} ${fol}
            wait
        done
        if [[ ${#Leps} -gt 3 ]]; then
            card=lep_20${yr}
            echo "> ${card}"
            tune ${card} _${card} ${fol}
            wait
        fi
    done
    if [[ ${#Years} -gt 2 && "$Years" != "16APV" ]]; then
        for lept in $Leps; do
            echo "> ${lept}"
            tune ${lept} _${lept} ${fol}
            wait
        done
        tune lep _lep ${fol}
        wait
    fi
    wait
    if [[ DoValidation -eq 1 && ClearValidation -eq 1 ]]; then
        echo "Removing $Val"
        rm -r ${Val}
        wait
    fi
    cd ..
    wait
done

for folpart in $FoldersComb; do
    fol=4D${folpart}
    echo $fol
    TopMass $fol $1 $2
    wait
    cd $fol
    wait
    if [[ -d $Res ]]; then
        echo "Removing $Res"
        rm ${Res}/*root
        wait
        rmdir $Res
        wait
    fi
    mkdir -p $Val
    wait
    for yr in $Years; do
        for lept in $Leps; do
            card=${lept}_20${yr}
            echo "> ${card}"
            tunecomb ${card} ${folpart} _${card} 4D
            wait
        done
        if [[ ${#Leps} -gt 3 ]]; then
            card=lep_20${yr}
            echo "> ${card}"
            tunecomb ${card} ${folpart} _${card} 4D
            wait
        fi
    done
    if [[ ${#Years} -gt 2 && "$Years" != "16APV" ]]; then
        for lept in $Leps; do
            echo "> ${lept}"
            tunecomb ${lept} ${folpart} _${lept} 4D
            wait
        done
        tunecomb lep ${folpart} _lep 4D
        wait
    fi
    wait
    if [[ DoValidation -eq 1 && ClearValidation -eq 1 ]]; then
        echo "Removing $Val"
        rm -r ${Val}
        wait
    fi
    cd ..
    wait
done

for folpart in $FoldersComb; do
    fol=2D${folpart}
    echo $fol
    TopMass $fol $1 $2
    wait
    cd $fol
    wait
    if [[ -d $Res ]]; then
        echo "Removing $Res"
        rm ${Res}/*root
        wait
        rmdir $Res
        wait
    fi
    mkdir -p $Val
    wait
    for yr in $Years; do
        for lept in $Leps; do
            card=${lept}_20${yr}
            echo "> ${card}"
            tunecomb ${card} ${folpart} _${card} 2D
            wait
        done
        if [[ ${#Leps} -gt 3 ]]; then
            card=lep_20${yr}
            echo "> ${card}"
            tunecomb ${card} ${folpart} _${card} 2D
            wait
        fi
    done
    if [[ ${#Years} -gt 2 && "$Years" != "16APV" ]]; then
        for lept in $Leps; do
            echo "> ${lept}"
            tunecomb ${lept} ${folpart} _${lept} 2D
            wait
        done
        tunecomb lep ${folpart} _lep 2D
        wait
    fi
    wait
    if [[ DoValidation -eq 1 && ClearValidation -eq 1 ]]; then
        echo "Removing $Val"
        rm -r ${Val}
        wait
    fi
    cd ..
    wait
done
