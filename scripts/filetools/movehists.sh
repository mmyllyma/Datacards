#!/bin/bash

#Folders="full half mw mlbr rbq 1D 2D 3D 3Db 4D 5D 5Db 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"
#Folders="full half mw mlbr rbq 1D 2D 3D 3Db 4D 5D 5Db mtwrbq mtw"
#Folders="mtwrbq"
#Folders="kdTree"
#Folders="mtwrbqlb"
Folders="5D"
#Folders="2D 2Dmtw 2Dmtrbq 2Dmtlbr 2Dmwrbq mtw mtrbq mtlbr mwrbq"
if [[ "$1" =~ ^(muo|ele)$ && "$2" =~ ^(16APV|16|17|18) ]]; then
    Name="${1}_20${2}"
    for fol in $Folders; do
        mkdir -p ${fol}
        mv histograms_${fol}.root ${fol}/${Name}.root
    done

    rm histograms_*.root
fi

