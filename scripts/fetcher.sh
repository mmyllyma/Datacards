#!/bin/bash
# Quickly append the desired suffix

FETCH=1 # Choose if files are fetch
MTOOL=1 # Multitoop
DCARD=1 # Run datacards

#YID=0 # 17 + 18
#YID=1 # 17
#YID=2 # 18
#YID=3 # 16
#YID=4 # 16APV
#YID=5 # 16APV + 16
YID=6 # full run2
LID=0 # ele + muo
#LID=1 # ele
#LID=2 # muo

if [[ $LID -eq 0 ]]; then
  Leps="muo ele"
elif [[ $LID -eq 1 ]]; then
  Leps="ele"
else
  Leps="muo"
fi

if [[ $YID -eq 0 ]]; then
  Years="17 18"
elif [[ $YID -eq 1 ]]; then
  Years="17"
elif [[ $YID -eq 2 ]]; then
  Years="18"
elif [[ $YID -eq 3 ]]; then
  Years="16"
elif [[ $YID -eq 4 ]]; then
  Years="16APV"
elif [[ $YID -eq 5 ]]; then
  Years="16APV 16"
elif [[ $YID -eq 6 ]]; then
  Years="16APV 16 17 18"
else
  Years="17"
fi
if [[ $FETCH -eq 1 ]]; then
  for year in $Years; do
      for lep in $Leps; do
          lept="muon"
          if [[ "${lep}" == "ele" ]]; then
              lept="electron"
          fi
          #rsync -avzh mmyllyma@naf-cms.desy.de:/afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/${lept}${year}/${1}bin/histo*root .

          cp /afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/${lept}${year}/${1}bin/histo*root .

          #cp /afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/backup/${lept}${year}/${1}bin/histo*root .
          #cp /afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/FSRscaledown/${lept}${year}/${1}bin/histo*root .
          #cp /afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/4Db_17bin/${lept}${year}/${1}bin/histo*root .


          #rsync -avzh mmyllyma@naf-cms.desy.de:/afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/backup/${lept}${year}_baseline/${1}bin/histo*root .
          #rsync -avzh mmyllyma@naf-cms.desy.de:/afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/backup/${lept}${year}_CR1ERD/${1}bin/histo*root .
          #rsync -avzh mmyllyma@naf-cms.desy.de:/afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/${lept}${year}_CR1ERDdataSyst/${1}bin/histo*root .
          #rsync -avzh mmyllyma@naf-cms.desy.de:/afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/${lept}${year}_pm3GeV_M1715/${1}bin/histo*root .
          #rsync -avzh mmyllyma@naf-cms.desy.de:/afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/${lept}${year}_pm3GeV_M1735/${1}bin/histo*root .
          #rsync -avzh mmyllyma@naf-cms.desy.de:/afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/realbackup/${lept}${year}_pm3GeV/${1}bin/histo*root .
          #rsync -avzh mmyllyma@naf-cms.desy.de:/afs/desy.de/user/m/mmyllyma/Work/leptonjets_run2_tmpUL16/C10629/src/TopMass/Analyzer/${lept}${year}run2flavor/${1}bin/histo*root .
          wait
          bash scripts/filetools/movehists.sh $lep $year
          wait
      done
  done
fi
if [[ $MTOOL -eq 1 ]]; then
  bash scripts/filetools/multitool.sh $LID $YID
fi
wait
if [[ $DCARD -eq 1 ]]; then
  bash scripts/filetools/datacards.sh $LID $YID
fi
