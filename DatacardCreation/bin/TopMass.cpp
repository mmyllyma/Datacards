#include <string>
#include <map>
#include <set>
#include <iostream>
#include <vector>
#include <utility>
#include <cstdlib>
#include "boost/filesystem.hpp"

#include "CombineHarvester/CombineTools/interface/CombineHarvester.h"
#include "CombineHarvester/CombineTools/interface/Utilities.h"
#include "CombineHarvester/CombineTools/interface/HttSystematics.h"
#include "CombineHarvester/CombineTools/interface/CardWriter.h"
#include "CombineHarvester/CombineTools/interface/CopyTools.h"
#include "CombineHarvester/CombineTools/interface/BinByBin.h"
#include "CombineHarvester/CombineTools/interface/Systematics.h"
#include "CombineHarvester/CombineTools/interface/Process.h"

using namespace std;

int main(int argc, char *argv[]) {
  ch::CombineHarvester cb;

  typedef vector<pair<int, string>> Categories;
  typedef vector<string> VString;

  const string endloc = argc < 2 ? "1D" : argv[1];
  const bool is_half = endloc.length()>4 && endloc.substr(endloc.length() - 4, 4) == "half";
  const bool is_full = endloc.length()>4 && endloc.substr(endloc.length() - 4, 4) == "full";
  const bool is_norm = !is_half && !is_full;
  const string baseloc = string(getenv("CMSSW_BASE")) + Form("/src/Datacards/%s/", endloc.c_str());
  const string normloc = string(getenv("CMSSW_BASE")) + Form("/src/Datacards/%s/", endloc.substr(0, endloc.length() - (is_norm ? 0 : 4)).c_str());
  const string altloc = string(getenv("CMSSW_BASE")) + Form("/src/Datacards/%s/", is_norm ? endloc.c_str() : (is_half ? "half" : "full"));
  const string proc_name = "mtop";

  const double coeffFSR = 1.;
  // const double coeffFSR = 2.;
  //
  //const bool simpleJEC = true;
  const bool simpleJEC = false;
  //
  //const bool simpleJER = true;
  const bool simpleJER = false;
  //
  //const bool simpleBTagSF = true;
  const bool simpleBTagSF = false;

  // Channel listings
  VString muo_chn = {"muo"};
  VString ele_chn = {"ele"};
  if (!is_norm) muo_chn.push_back("muomlb");
  if (!is_norm) ele_chn.push_back("elemlb");
  // Current channels
  const int lid = argc < 3 ? 2 : std::stoi(string(argv[2]));
  VString chns = {"muo"};
  if (lid == 1) chns = {"ele"};
  else if (lid == 0) chns = {"muo", "ele"};

  const int yid = argc < 4 ? 1 : std::stoi(string(argv[3]));
  VString eras = {"2017"};
  if (yid == 2) eras = {"2018"};
  else if (yid == 3) eras = {"2016"};
  else if (yid == 4) eras = {"2016APV"};
  else if (yid == 5) eras = {"2016APV", "2016"};
  else if (yid == 6) eras = {"2016APV", "2016", "2017", "2018"};
  else if (yid == 0) eras = {"2017", "2018"};

  const VString sig_procs = {"tt-semil", "tt-dilept", "st-tW", "st-tchan"};
  const VString bkg_procs = {"qcd", "w+jets", "dy+jets", "tt+V", "st-schan", "ww", "wz", "zz", "tt-allhad"};

  //const bool do17 = std::find(eras.begin(), eras.end(), "2017") != eras.end();
  //const bool do18 = std::find(eras.begin(), eras.end(), "2018") != eras.end();
  const bool doMuo = std::find(chns.begin(), chns.end(), "muo") != chns.end(); 
  const bool doEle = std::find(chns.begin(), chns.end(), "ele") != chns.end(); 

  cout << ">> Creating processes and observations...\n";
  for (const auto &era : eras) {
    for (const auto &chn : chns) {
      const string catName = chn + "_" + era;
      cb.AddObservations({"*"}, {proc_name}, {era}, {chn}, {{0, catName}});
      cb.AddProcesses({"*"}, {proc_name}, {era}, {chn}, bkg_procs, {{0, catName}}, false);
      cb.AddProcesses({"*"}, {proc_name}, {era}, {chn}, sig_procs, {{0, catName}}, true);
      if (!is_norm) {
        const string altChn = chn + "mlb";
        const string altName = altChn + "_" + era;
        cb.AddObservations({"*"}, {proc_name}, {era}, {altChn}, {{0, altName}});
        cb.AddProcesses({"*"}, {proc_name}, {era}, {altChn}, bkg_procs, {{0, altName}}, false);
        cb.AddProcesses({"*"}, {proc_name}, {era}, {altChn}, sig_procs, {{0, altName}}, true);
      }
    }
  }

  cout << ">> Adding systematic uncertainties...\n";
  // Luminosity scales!
  // source: https://twiki.cern.ch/twiki/bin/viewauth/CMS/TopSystematics#Luminosity
  // same uncertainty for both years and fully correlated between 2016pre and postVFP
  cb.cp().era({"2016APV"}).AddSyst(cb, "lumi_13TeV_2016", "lnN", ch::syst::SystMap<>::init(1.010));
  cb.cp().era({"2016"}).AddSyst(cb, "lumi_13TeV_2016", "lnN", ch::syst::SystMap<>::init(1.010));
  cb.cp().era({"2017"}).AddSyst(cb, "lumi_13TeV_2017", "lnN", ch::syst::SystMap<>::init(1.020));
  cb.cp().era({"2018"}).AddSyst(cb, "lumi_13TeV_2018", "lnN", ch::syst::SystMap<>::init(1.015));
  cb.cp().AddSyst(cb, "lumi_13TeV_1718", "lnN", ch::syst::SystMap<ch::syst::era>::init
      ({"2017"}, 1.006)
      ({"2018"}, 1.002));
  cb.cp().AddSyst(cb, "lumi_13TeV_correlated", "lnN", ch::syst::SystMap<ch::syst::era>::init
      ({"2016APV"}, 1.006)
      ({"2016"}, 1.006)
      ({"2017"}, 1.009)
      ({"2018"}, 1.020));
  // Only ttbar is given an xsec-error, while others have more loose uncertainties. Should we relax this condition?
  cb.cp().process({"tt-semil", "tt-dilept", "tt-allhad"}).AddSyst(cb, "xsec_ttbar", "lnN", ch::syst::SystMapAsymm<>::init(0.94514, 1.04839));
  // Normalizations
  cb.cp().process({"tt-dilept"})                         .AddSyst(cb, "norm_dilept", "lnN", ch::syst::SystMapAsymm<>::init(0.95, 1.05));
  cb.cp().process({"st-tW"})                             .AddSyst(cb, "norm_st-tW", "lnN", ch::syst::SystMapAsymm<>::init(0.94, 1.06));
  cb.cp().process({"st-tchan"})                          .AddSyst(cb, "norm_st-tchan", "lnN", ch::syst::SystMapAsymm<>::init(0.92, 1.08));
  cb.cp().process({"st-schan"})                          .AddSyst(cb, "norm_st-schan", "lnN", ch::syst::SystMapAsymm<>::init(0.92, 1.08));
  cb.cp().process({"w+jets"})                            .AddSyst(cb, "norm_wjets", "lnN", ch::syst::SystMapAsymm<>::init(0.9, 1.1));
  cb.cp().process({"dy+jets"})                           .AddSyst(cb, "norm_dy", "lnN", ch::syst::SystMapAsymm<>::init(0.9, 1.1));
  cb.cp().process({"tt+V"})                              .AddSyst(cb, "norm_tt+V", "lnN", ch::syst::SystMapAsymm<>::init(0.9, 1.1));
  // We separate the muon and electron normalizations, as the MC modeling is quite different in these.
  if (doMuo) {
    cb.cp().channel(muo_chn).process({"qcd"}).AddSyst(cb, "norm_qcdmuo", "lnN", ch::syst::SystMapAsymm<>::init(0.5,2.0));
  }
  if (doEle) {
    // The current electron QCD used involved techniques due to low statistics => increased error estimates.
    cb.cp().channel(ele_chn).process({"qcd"}).AddSyst(cb, "norm_qcdele", "lnN", ch::syst::SystMapAsymm<>::init(0.5,2.0));
  }

  cb.cp().process({"tt-semil", "tt-dilept", "st-tW", "st-tchan"}).AddSyst(cb, "MTop", "shapeU", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FSRx2xg", "shape", ch::syst::SystMap<>::init(coeffFSR));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FSRq2qg", "shape", ch::syst::SystMap<>::init(coeffFSR));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FSRg2gg", "shape", ch::syst::SystMap<>::init(coeffFSR));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FSRg2qq", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FSRCNSx2xg", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FSRCNSq2qg", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FSRCNSg2gg", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FSRCNSg2qq", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept"}).AddSyst(cb, "ISR", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"st-tW"}).AddSyst(cb, "ISRst", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "L1Prefire", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FactScale", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "RenScale", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "Pileup", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BFragBL", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BFragSL", "shape", ch::syst::SystMap<>::init(1.0));
  if (simpleBTagSF) {
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BTagCSF", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BTagSF", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BMisTagSF", "shape", ch::syst::SystMap<>::init(1.0));
  } else {
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BCTagSF_corr", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BMisTagSF_corr", "shape", ch::syst::SystMap<>::init(1.0));

    // Notice this kind of handling for all year-dependent systematics, need to be confirmed

    //cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BCTagSF_$ERA", "shape", ch::syst::SystMap<>::init(1.0)); 
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "BCTagSF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb,    "BCTagSF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb,    "BCTagSF_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb,    "BCTagSF_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BMisTagSF_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "BMisTagSF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb,    "BMisTagSF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb,    "BMisTagSF_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb,    "BMisTagSF_2018", "shape", ch::syst::SystMap<>::init(1.0));

  }
  // One-sided systematics: down-variation equal to the central value.
  // Having both Peterson and BL central weights is unwise, as these both correspond to the LEP fit central value.
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BFragPeterson", "shape", ch::syst::SystMap<>::init(1.0));
  //cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "BFragBLs", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept"}).AddSyst(cb, "TopPt", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept"}).AddSyst(cb, "AlphaS", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept"}).AddSyst(cb, "PDF", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept"}).AddSyst(cb, "CT14", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept"}).AddSyst(cb, "MMHT2014", "shape", ch::syst::SystMap<>::init(1.0));


  // Flavor-JES: 100 % correlated accross the years
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FlavorPureBottom", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FlavorPureCharm", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FlavorPureQuark", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "FlavorPureGluon", "shape", ch::syst::SystMap<>::init(1.0));
  if (simpleJEC) {
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECIntercalibration", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECMPFInSitu", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECUncorrelated_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECUncorrelated_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb,    "JECUncorrelated_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb,    "JECUncorrelated_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb,    "JECUncorrelated_2018", "shape", ch::syst::SystMap<>::init(1.0));

  } else {
    // For JEC correlations, check https://docs.google.com/spreadsheets/d/1JZfk78_9SD225bcUuTWVo4i02vwI5FfeVKH-dwzUdhM/edit#gid=1345121349
    // 100 % correlated JEC cases
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECAbsoluteMPFBias", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECAbsoluteScale", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECFragmentation", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECSinglePionECAL", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECSinglePionHCAL", "shape", ch::syst::SystMap<>::init(1.0));
    // 50 % correlated JEC cases (approximated by 100 % for now)
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECPileUpDataMC", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECPileUpPtBB", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECPileUpPtEC1", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECPileUpPtEC2", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECPileUpPtRef", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECPileUptPtHF", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativeFSR", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativeJERHF", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativePtBB", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativePtHF", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativeBal", "shape", ch::syst::SystMap<>::init(1.0));
    // 0% correlated JEC cases

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECAbsoluteStat_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECAbsoluteStat_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECAbsoluteStat_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECAbsoluteStat_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECAbsoluteStat_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativeJEREC1_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECRelativeJEREC1_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECRelativeJEREC1_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECRelativeJEREC1_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECRelativeJEREC1_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativeJEREC2_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECRelativeJEREC2_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECRelativeJEREC2_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECRelativeJEREC2_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECRelativeJEREC2_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativePtEC1_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECRelativePtEC1_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECRelativePtEC1_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECRelativePtEC1_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECRelativePtEC1_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativePtEC2_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECRelativePtEC2_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECRelativePtEC2_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECRelativePtEC2_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECRelativePtEC2_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativeSample_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECRelativeSample_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECRelativeSample_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECRelativeSample_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECRelativeSample_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativeStatEC_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECRelativeStatEC_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECRelativeStatEC_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECRelativeStatEC_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECRelativeStatEC_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativeStatFSR_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECRelativeStatFSR_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECRelativeStatFSR_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECRelativeStatFSR_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECRelativeStatFSR_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECRelativeStatHF_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECRelativeStatHF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECRelativeStatHF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECRelativeStatHF_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECRelativeStatHF_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JECTimePtEta_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JECTimePtEta_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JECTimePtEta_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JECTimePtEta_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JECTimePtEta_2018", "shape", ch::syst::SystMap<>::init(1.0));
  }
  if (simpleJER) {
    //cb.cp().process({"tt-semil"}).AddSyst(cb, "JER_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "JER_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2016"}).AddSyst(cb, "JER_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2017"}).AddSyst(cb, "JER_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil"}).era({"2018"}).AddSyst(cb, "JER_2018", "shape", ch::syst::SystMap<>::init(1.0));

  } else {
    //cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "JERInner_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "JERInner_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb, "JERInner_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb, "JERInner_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb, "JERInner_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "JERMid_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "JERMid_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb, "JERMid_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb, "JERMid_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb, "JERMid_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "JEROuter_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "JEROuter_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb, "JEROuter_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb, "JEROuter_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb, "JEROuter_2018", "shape", ch::syst::SystMap<>::init(1.0));
  }
  //cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "MET_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "MET_2016", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb, "MET_2016", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb, "MET_2017", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb, "MET_2018", "shape", ch::syst::SystMap<>::init(1.0));

  cb.cp().process({"tt-semil"}).AddSyst(cb, "HDAMP", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil"}).AddSyst(cb, "UETune", "shape", ch::syst::SystMap<>::init(1.0));

  // One-sided systematics: down-variation equal to the central value originally, modified later to two-sided systematics.
  cb.cp().process({"tt-semil"}).AddSyst(cb, "CRQCDBased", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil"}).AddSyst(cb, "CRGluonMove", "shape", ch::syst::SystMap<>::init(1.0));
  cb.cp().process({"tt-semil"}).AddSyst(cb, "ERD", "shape", ch::syst::SystMap<>::init(1.0));
  //cb.cp().process({"tt-semil"}).AddSyst(cb, "CRQCDBasedERD", "shape", ch::syst::SystMap<>::init(1.0));

  // Different tests
  //cb.cp().process({"tt-semil"}).AddSyst(cb, "HDAMPS1", "shape", ch::syst::SystMap<>::init(1.0));
  //cb.cp().process({"tt-semil"}).AddSyst(cb, "UETuneS1", "shape", ch::syst::SystMap<>::init(1.0));
  //cb.cp().process({"tt-semil"}).AddSyst(cb, "CRQCDBasedS1", "shape", ch::syst::SystMap<>::init(1.0));
  //cb.cp().process({"tt-semil"}).AddSyst(cb, "CRGluonMoveS1", "shape", ch::syst::SystMap<>::init(1.0));
  //cb.cp().process({"tt-semil"}).AddSyst(cb, "ERDS1", "shape", ch::syst::SystMap<>::init(1.0));
  //cb.cp().process({"tt-semil"}).AddSyst(cb, "RTT", "shape", ch::syst::SystMap<>::init(1.0));

  if (doMuo) {
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "MuoIDSF", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "MuoIsoSF", "shape", ch::syst::SystMap<>::init(1.0));
    //cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "MuoIDSFStat_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "MuoIDSFStat_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb, "MuoIDSFStat_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb, "MuoIDSFStat_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb, "MuoIDSFStat_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "MuoIsoSFStat_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "MuoIsoSFStat_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb, "MuoIsoSFStat_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb, "MuoIsoSFStat_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb, "MuoIsoSFStat_2018", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "MuoTrgSF_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "MuoTrgSF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb, "MuoTrgSF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb, "MuoTrgSF_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb, "MuoTrgSF_2018", "shape", ch::syst::SystMap<>::init(1.0));

    cb.cp().channel(muo_chn).process({"tt-semil"}).AddSyst(cb, "MuoScaleEWK", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil"}).AddSyst(cb, "MuoScaleEWK2", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil"}).AddSyst(cb, "MuoScaleZPt", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil"}).AddSyst(cb, "MuoScaleDM", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().channel(muo_chn).process({"tt-semil"}).AddSyst(cb, "MuoScaleStat_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "MuoScaleStat_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil"}).era({"2016"}).AddSyst(cb, "MuoScaleStat_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil"}).era({"2017"}).AddSyst(cb, "MuoScaleStat_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(muo_chn).process({"tt-semil"}).era({"2018"}).AddSyst(cb, "MuoScaleStat_2018", "shape", ch::syst::SystMap<>::init(1.0));
  }
  if (doEle) {
    cb.cp().channel(ele_chn).process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "EleIDSF", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(ele_chn).process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "EleRecoSF", "shape", ch::syst::SystMap<>::init(1.0));

    //cb.cp().channel(ele_chn).process({"tt-semil", "tt-dilept", "st-tW"}).AddSyst(cb, "EleTrgSF_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(ele_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016APV"}).AddSyst(cb, "EleTrgSF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(ele_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2016"}).AddSyst(cb, "EleTrgSF_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(ele_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2017"}).AddSyst(cb, "EleTrgSF_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(ele_chn).process({"tt-semil", "tt-dilept", "st-tW"}).era({"2018"}).AddSyst(cb, "EleTrgSF_2018", "shape", ch::syst::SystMap<>::init(1.0));

    cb.cp().channel(ele_chn).process({"tt-semil"}).AddSyst(cb, "EleReso", "shape", ch::syst::SystMap<>::init(1.0));
    
    //cb.cp().channel(ele_chn).process({"tt-semil"}).AddSyst(cb, "EleScale_$ERA", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(ele_chn).process({"tt-semil"}).era({"2016APV"}).AddSyst(cb, "EleScale_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(ele_chn).process({"tt-semil"}).era({"2016"}).AddSyst(cb, "EleScale_2016", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(ele_chn).process({"tt-semil"}).era({"2017"}).AddSyst(cb, "EleScale_2017", "shape", ch::syst::SystMap<>::init(1.0));
    cb.cp().channel(ele_chn).process({"tt-semil"}).era({"2018"}).AddSyst(cb, "EleScale_2018", "shape", ch::syst::SystMap<>::init(1.0));
  }

  for (const auto &era : eras) {
    // Extracting and checking histograms
    for (const string &chn : chns) {
      const string file = normloc + chn + "_" + era + ".root";
      cb.cp().channel({chn}).era({era}).backgrounds().ExtractShapes(file, "$PROCESS", "$PROCESS_$SYSTEMATIC");
      cb.cp().channel({chn}).era({era}).signals().ExtractShapes(file, "$PROCESS", "$PROCESS_$SYSTEMATIC");
      if (!is_norm) {
        const string altchn = chn + "mlb";
        const string file = altloc + chn + "_" + era + ".root";
        cb.cp().channel({altchn}).era({era}).backgrounds().ExtractShapes(file, "$PROCESS", "$PROCESS_$SYSTEMATIC");
        cb.cp().channel({altchn}).era({era}).signals().ExtractShapes(file, "$PROCESS", "$PROCESS_$SYSTEMATIC");
      }
    }
    // Setting rates to be fetched from histograms (signal)
    for (string const& pr : sig_procs) {
      cb.cp().process({pr}).era({era}).ForEachProc([&](ch::Process *proc) {
        proc->set_rate(-1);
      });
    }
    // Setting rates to be fetched from histograms (bkg)
    for (string const& pr : bkg_procs) {
      cb.cp().process({pr}).era({era}).ForEachProc([&](ch::Process *proc) {
        proc->set_rate(-1);
      });
    }
  }

  // Example of various choises one can make:
  //cb.cp().process({"ggH"}).bin_id({3, 6, 7})
  //    .AddSyst(cb, "QCDscale_ggH2in_$BIN_$ERA", "lnN", SystMap<channel, era, bin_id>::init
  //    ({"mt"}, {"7TeV", "8TeV"},  {6}, 1.228)
  //    ({"et"}, {"7TeV"},          {6}, 1.275));
  
  // Bin-by-bin mergin example:
  //auto bbb = ch::BinByBinFactory()
  //    .SetAddThreshold(0.1)
  //    .SetMergeThreshold(0.5)
  //    .SetFixNorm(true);
  //ch::CombineHarvester cb_et = cb.cp().channel({"et"});
  //bbb.MergeAndAdd(cb_et.cp().era({"8TeV"}).bin_id({1, 2}).process({"ZL", "ZJ", "QCD", "W"}), cb);
  //bbb.MergeAndAdd(cb_et.cp().era({"8TeV"}).bin_id({7}).process({"ZL", "ZJ", "W", "ZTT"}), cb);
  //ch::SetStandardBinNames(cb);

  // "Modern" cardwriter example.
  //ch::CardWriter writer("$TAG/$ANALYSIS_$CHANNEL_$BINID_$ERA.txt", "$TAG/$ANALYSIS_$CHANNEL.input_$ERA.root");
  // writer.SetVerbosity(1);
  //writer.WriteCards("jaa", cb);
  //for (auto chn : cb.channel_set()) {
  //  cout << chn << endl;
  //  writer.WriteCards("juu" + chn, cb.cp().channel({chn}));
  //}

  // Writing datacards manually - easier with CardWriter below:
  const string folder = baseloc + "results";
  boost::filesystem::create_directories(folder);
  for (const string &era : eras) {
    for (const string &chn : chns) {
      const string bin = chn + "_" + era;
      TFile output((folder + "/out_" + bin + ".root").c_str(), "RECREATE");
      cout << ">> Writing datacard for bin: " << bin << endl;
      if (is_norm) {
        cb.cp().era({era}).channel({chn}).WriteDatacard(baseloc + bin + ".txt", output);
      } else {
        const string altchn = chn + "mlb";
        cb.cp().era({era}).channel({chn, altchn}).WriteDatacard(baseloc + bin + ".txt", output);
      }
      output.Close();
    }
    if (chns.size() > 1) {
      TFile output((folder + "/out_lep_" + era + ".root").c_str(), "RECREATE");
      cb.cp().era({era}).WriteDatacard(baseloc + "lep_" + era + ".txt", output);
      output.Close();
    }
  }
  // No sense in printing out a full file if we have only a single era. 
  if (eras.size() > 1) {
    for (const string &chn : chns) {
      TFile output((folder + "/out_" + chn + ".root").c_str(), "RECREATE");
      if (is_norm) {
        cb.cp().channel({chn}).WriteDatacard(baseloc + chn + ".txt", output);
      } else {
        const string altchn = chn + "mlb";
        cb.cp().channel({chn, altchn}).WriteDatacard(baseloc + chn + ".txt", output);
      }
      output.Close();
    }
    {
      TFile output((folder + "/out_lep.root").c_str(), "RECREATE");
      cb.cp().WriteDatacard(baseloc + "lep.txt", output);
      output.Close();
    }
  }

  cout << "\n>> Done!\n";
}
